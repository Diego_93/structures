package aa;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;


public class Solution{

	public void solve() {
		// Here write your code
	}
	
	
	
	
	
	
	
	
	InputStream is;
	StringBuilder out;
	String INPUT = "";
	
	void run() throws Exception
	{
		is = INPUT.isEmpty() ? System.in : new ByteArrayInputStream(INPUT.getBytes());
		out = new StringBuilder();
		long s = System.currentTimeMillis();
		solve();
		System.out.println(out.toString());
		if(!INPUT.isEmpty())tr(System.currentTimeMillis()-s+"ms");
	}
	
	public static void main(String[] args) throws Exception {
		new Solution().run();
	}
	
	private byte[] inbuf = new byte[1024];
	private int lenbuf = 0, ptrbuf = 0;
	
	private int readByte()
	{
		if(lenbuf == -1)throw new InputMismatchException();
		if(ptrbuf >= lenbuf){
			ptrbuf = 0;
			try { lenbuf = is.read(inbuf); } catch (IOException e) { throw new InputMismatchException(); }
			if(lenbuf <= 0)return -1;
		}
		return inbuf[ptrbuf++];
	}
	
	private boolean isSpaceChar(int c) { return !(c >= 33 && c <= 126); }
	private int skip() { int b; while((b = readByte()) != -1 && isSpaceChar(b)); return b; }
	
	private double nd() { return Double.parseDouble(ns()); }
	private char nc() { return (char)skip(); }
	
	private String ns()
	{
		int b = skip();
		StringBuilder sb = new StringBuilder();
		while(!(isSpaceChar(b))){ // when nextLine, (isSpaceChar(b) && b != ' ')
			sb.appendCodePoint(b);
			b = readByte();
		}
		return sb.toString();
	}
	
	private char[] ns(int n)
	{
		char[] buf = new char[n];
		int b = skip(), p = 0;
		while(p < n && !(isSpaceChar(b))){
			buf[p++] = (char)b;
			b = readByte();
		}
		return n == p ? buf : Arrays.copyOf(buf, p);
	}
	
	private char[][] nm(int n, int m)
	{
		char[][] map = new char[n][];
		for(int i = 0;i < n;i++)map[i] = ns(m);
		return map;
	}
	
	private long[] nla(int n)
	{
		long[] a = new long[n];
		for(int i = 0;i < n;i++)a[i] = nl();
		return a;
	}
	
	private int[] nia(int n)
	{
		int[] a = new int[n];
		for(int i = 0;i < n;i++)a[i] = ni();
		return a;
	}
	
	private int ni()
	{
		int num = 0, b;
		boolean minus = false;
		while((b = readByte()) != -1 && !((b >= '0' && b <= '9') || b == '-'));
		if(b == '-'){
			minus = true;
			b = readByte();
		}
		
		while(true){
			if(b >= '0' && b <= '9'){
				num = num * 10 + (b - '0');
			}else{
				return minus ? -num : num;
			}
			b = readByte();
		}
	}
	
	private long nl()
	{
		long num = 0;
		int b;
		boolean minus = false;
		while((b = readByte()) != -1 && !((b >= '0' && b <= '9') || b == '-'));
		if(b == '-'){
			minus = true;
			b = readByte();
		}
		
		while(true){
			if(b >= '0' && b <= '9'){
				num = num * 10 + (b - '0');
			}else{
				return minus ? -num : num;
			}
			b = readByte();
		}
	}
	
	private void forn(int n, Consumer<Integer> c){
		for(int i=0; i<n; i++) c.accept(i);
	}
	
	private void forn(int[] a, BiConsumer<Integer, Integer> c){
		for(int i=0; i<a.length; i++) c.accept(i,a[i]);
	} 
	
	private void forn(int[] a, Consumer<Integer> c){
		for(int i=0; i<a.length; i++) c.accept(a[i]);
	}
	
	private void forn(long[] a, BiConsumer<Integer,Long> c){
		for(int i=0; i<a.length; i++) c.accept(i,a[i]);
	}
	
	private void forn(long[] a, Consumer<Long> c){
		for(int i=0; i<a.length; i++) c.accept(a[i]);
	}

	private <T> T[] map(int[] a, Function<Integer, T> f){
		T[] t = (T[]) new Object[a.length];
		for(int i=0; i<a.length; i++){
			t[i] = f.apply(a[i]);
		}
		return t;
	}
	
	private <T> T[] map(long[] a, Function<Long, T> f){
		T[] t = (T[]) new Object[a.length];
		for(int i=0; i<a.length; i++){
			t[i] = f.apply(a[i]);
		}
		return t;
	}

	private <T,S> T[] map(S[] a, Function<S, T> f){
		T[] t = (T[]) new Object[a.length];
		for(int i=0; i<a.length; i++){
			t[i] = f.apply(a[i]);
		}
		return t;
	}
	
	private static void tr(Object... o) { System.out.println(Arrays.deepToString(o)); }
}
