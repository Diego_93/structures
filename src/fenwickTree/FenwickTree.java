package fenwickTree;

public class FenwickTree {

	long[] acumValues;
	long[] values;
	
	public FenwickTree(int N){
		acumValues = new long[N];
		values = new long[N];
	}
	
	public long get(int n){
		return values[n+1];
	}
	
	public long getAcum(int l, int r){
		if(l==0)
			return getAcum(r);
		return getAcum(r)-getAcum(l-1);
	}
	
	public long getAcum(int n){
		n++;
		long sum=0;
		for(;n>0;n-=(n&-n)){
			sum+=acumValues[n-1];
		}
		return sum;
	}
	
	public void set(int n, long value){
		n++;
		long val = value-values[n];
		update(n, val);
	}
	
	public void update(int n, long value){
		n++;
		values[n]+=value; 
		for(; n<=acumValues.length;n+=(n&-n)){
			acumValues[n-1]+=value; //Suma
		}
	}
}
