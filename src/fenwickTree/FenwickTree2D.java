package fenwickTree;


public class FenwickTree2D {

  private long[][] t;	
  
  public FenwickTree2D(int a, int b){
	  t = new long[a][b];
  }
	
  public void add(int x, int y, long value) {
    for (int i = x; i < t.length; i |= i + 1)
      for (int j = y; j < t[0].length; j |= j + 1)
        t[i][j] += value;
  }

  /**
   * sum[(0, 0), (x, y)]
   */
  public long sum(int x, int y) {
    long res = 0;
    for (int i = x; i >= 0; i = (i & (i + 1)) - 1)
      for (int j = y; j >= 0; j = (j & (j + 1)) - 1)
        res += t[i][j];
    return res;
  }

  /**
   *  sum[(x1, y1), (x2, y2)]
   */
  public long sum(int x1, int y1, int x2, int y2) {
    return sum(x2, y2) - sum(x1 - 1, y2) - sum(x2, y1 - 1) + sum(x1 - 1, y1 - 1);
  }

  public long get(int x, int y) {
    return sum(x, y, x, y);
  }

  public void set(int x, int y, long value) {
    add(x, y, -get(x, y) + value);
  }
}
