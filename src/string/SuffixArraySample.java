package string;



public class SuffixArraySample {

	
	public static void main(String[] args) {
		
		String str = "";
		
		for(int t=1; t<20; t++){
			for(int i=0; i<5000; i++){
				char ch =(char)('a'+i%15);
				str += Character.toString(ch);
			}
			long init = System.currentTimeMillis();
			int[] sa = SuffixArray.suffixArray(str);
			
			long fin = System.currentTimeMillis();
			System.out.println(t*5000+" "+(fin-init)+" "+((100000.0*(fin-init))/complex(t*5000)));	
		}
		str = "abracadabra";
		int[] sa = SuffixArray.suffixArray(str);
		for(int i=0; i<sa.length; i++){
			System.out.println(str.substring(sa[i]));
		}
	}
	
	public static double complex(int n){
		return Math.log(n)*n;
	}

}
