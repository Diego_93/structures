package string;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class KMP {

	  public static int[] prefixFunction(String s) {
	    int[] p = new int[s.length()];
	    int k = 0;
	    for (int i = 1; i < s.length(); i++) {
	      while (k > 0 && s.charAt(k) != s.charAt(i))
	        k = p[k - 1];
	      if (s.charAt(k) == s.charAt(i))
	        ++k;
	      p[i] = k;
	    }
	    return p;
	  }

	  public static int kmpMatcher(String s, String pattern) {
	    int m = pattern.length();
	    if (m == 0)
	      return 0;
	    int[] p = prefixFunction(pattern);
	    for (int i = 0, k = 0; i < s.length(); i++)
	      for (; ; k = p[k - 1]) {
	        if (pattern.charAt(k) == s.charAt(i)) {
	          if (++k == m)
	            return i + 1 - m;
	          break;
	        }
	        if (k == 0)
	          break;
	      }
	    return -1;
	  }
	  
	  public static List<Integer> kmpMatcherAllNoOverlap(String s, String pattern){
		  List<Integer> list = new LinkedList<Integer>();
		  int m = pattern.length();
		    if (m == 0)
		      return list;
		    int[] p = prefixFunction(pattern);
		    for (int i = 0, k = 0; i < s.length(); i++)
		      for (; ; k = p[k - 1]) {
		        if (pattern.charAt(k) == s.charAt(i)) {
		          if (++k == m){
		        	 list.add(i+1-m);
		        	 k=0;
		          }
		          break;
		        }
		        if (k == 0)
		          break;
		      }
		    return list;
	  }
	  
	  public static List<Integer> kmpMatcherAllWithOverlap(String s, String pattern){
		  List<Integer> list = new LinkedList<Integer>();
		  int m = pattern.length();
		    if (m == 0)
		      return list;
		    int[] p = prefixFunction(pattern);
		    for (int i = 0, k = 0; i < s.length(); i++)
		      for (; ; k = p[k - 1]) {
		        if (pattern.charAt(k) == s.charAt(i)) {
		          if (++k == m){
		        	 list.add(i+1-m);
		        	 k=p[k-1];
		        	 break;
		          }
		          break;
		        }
		        if (k == 0)
		          break;
		      }
		    return list;
	  }
}
