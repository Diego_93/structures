
import java.io.*;
import java.util.*;
import java.util.function.Consumer;

public class Main {
	   
    public static void run() {
		int k = getInt();
		int r = getInt();
		for(int i=1; i<=10; i++){
			if((k*i-r+10) % 10==0 || (k*i) % 10==0){
				System.out.println(i);
				return;
			}
		}
	}
    
    
    static Scanner s;
	static PrintWriter out;
    static Scanner newInput() throws IOException {
        if (System.getProperty("JUDGE") != null) {
            return new Scanner(new File("aplusb.in"));
        } else {
            return new Scanner(System.in);
        }
    }
    static public int getInt(){ return s.nextInt(); }
	static public long getLong(){ return s.nextLong(); }
	static public String getString(){ return s.next();}
	static public Integer[] getArrayInt(int size){
		Integer[] ans = new Integer[size];
		for(int i=0; i<size; i++)
			ans[i]=getInt();
		return ans;
	}
	static public Long[] getArrayLong(int size){
		Long[] ans = new Long[size];
		for(int i=0; i<size; i++)
			ans[i]=getLong();
		return ans;
	}
	static public void forn(String str, Consumer<Character> c){	
		for(int i=0; i<str.length(); i++){
			c.accept(str.charAt(i));
		}
	}
	static public <T> void forn(T[] arr, Consumer<T> c){
		for(int i=0; i<arr.length; i++){
			c.accept(arr[i]);
		}
	}
    static PrintWriter newOutput() throws IOException {
        if (System.getProperty("JUDGE") != null) {
            return new PrintWriter("aplusb.out");
        } else {
            return new PrintWriter(System.out);
        }
    }
    public static void main(String[] args) throws IOException {
    	s = newInput();
    	out = newOutput();
        run();
    }

}