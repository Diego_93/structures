package rmq;

import java.util.Arrays;
import java.util.function.BiFunction;


public class RMQSample {

	public static void main(String[] args) {
		Integer[] arr = {1,1,1,1,1,1,1,1,1,1,1,1};
		RMQ<Integer> rmq = new RMQ<Integer>((x,y)->x+y, arr);
		System.out.println(rmq);
		System.out.println(rmq.query(0, rmq.size()-1));
		rmq.add(2);
		rmq.add(2);
		rmq.add(2);
		rmq.add(2);
		rmq.add(2);
		rmq.add(2);
		System.out.println(rmq);
		System.out.println(rmq.query(0, 11));
		System.out.println(rmq.query(0, rmq.size()-1));
		rmq.put(9, 10);
		System.out.println(rmq);
		System.out.println(rmq.query(0, 11));
		System.out.println(rmq.query(0, rmq.size()-1));
		System.out.println("--------------------------");
		
		long[] barr= {1,1,1,1,1,1,1,1,1,1,1,1};
		RMQWithDelay r = new RMQWithDelay((x,y)->x+y, (x,y)->x*y, barr);
		System.out.println(r.query(0, r.size()-1));
		System.out.println(r);
		r.addToRange(1, 1,  4);
		System.out.println(r.query(0, r.size()-1));
		System.out.println(r);
		r.addToRange(2, 3, 2);
		System.out.println(r.query(0, r.size()-1));
		System.out.println(r);
		System.out.println(r.query(2, 3));
		System.out.println(r);
		System.out.println(r.query(2, 2));
		System.out.println(r);
		
		System.out.println("--------------------------");
		
		long[] maxs= {2,3,8,4,6,7};
		RMQWithDelay q = new RMQWithDelay((x,y)->Math.max(x,y), (x,y)->y, maxs);
		System.out.println(q.query(0, q.size()-1));
		System.out.println(q.query(0, 2));
		System.out.println(q.query(4, 5));
		System.out.println(q);
		q.addToRange(0, 1, 8);
		System.out.println(q.query(0, q.size()-1));
		System.out.println(q.query(0, 2));
		System.out.println(q.query(4, 5));
		System.out.println(q);
		q.addToRange(1, 1, -5);
		System.out.println(q.query(0, q.size()-1));
		System.out.println(q.query(0, 2));
		System.out.println(q.query(4, 5));
		System.out.println(q);
		
		System.out.println("--------------------------");
	}
}
