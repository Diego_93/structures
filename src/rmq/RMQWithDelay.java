package rmq;

import java.util.function.BiFunction;

public class RMQWithDelay {

	private int maxCap, size;
	private Long[] data;
	private Long[] modification;
	private BiFunction<Long, Long, Long> op;
	private BiFunction<Long, Long, Long> modifyOp;
	private static final int STARTCAP = 64;

	@SuppressWarnings("unchecked")
	public RMQWithDelay(BiFunction<Long, Long, Long> op,
			BiFunction<Long, Long, Long> modifyOp, long[] data) {
		this.op = op;
		this.modifyOp = modifyOp;
		size = data.length;
		maxCap = STARTCAP;
		while (maxCap < data.length)
			maxCap *= 2;
		this.data = new Long[maxCap * 2];
		this.modification = new Long[maxCap * 2];
		for (int i = 0; i < data.length; i++) {
			this.data[maxCap - 1 + i] = data[i];
		}
		refresh();
	}

	private void refresh() {
		for (int i = maxCap - 2; i >= 0; i--) {
			if (this.data[2 * i + 1] == null) {
				this.data[i] = this.data[2 * i + 2];
				continue;
			}
			if (this.data[2 * i + 2] == null) {
				this.data[i] = this.data[2 * i + 1];
				continue;
			}
			this.data[i] = op.apply(this.data[2 * i + 1], this.data[2 * i + 2]);
		}
	}
	
	public void add(int index, long value){
		addToRange(index, index, value);
	}

	public void addToRange(int left, int right, long value) {
		addToRange(0, 0, maxCap - 1, left, right, value);
	}

	private void addToRange(int i, int li, int ri, int lo, int ro, long v) {
		if(lo<=li && ri<= ro){
			data[i]+=modifyOp.apply((long)(ri-li+1), v);
			if(li!=ri){
				modification[i]=(modification[i]==null?v:modification[i]+v);
			}else{
				
			}
			return;
		}
		if(modification[i]!=null){
			propagateModification(i, ri-li+1);
		}
		int mid = (ri+li)/2;
		if(lo>mid){
			addToRange(i*2+2, mid+1, ri, lo, ro, v);
		}else if(ro<=mid){
			addToRange(i*2+1, li, mid, lo, ro, v);
		}else{
			addToRange(i*2+2, mid+1, ri, lo, ro, v);
			addToRange(i*2+1, li, mid, lo, ro, v);
		}
		if(data[2*i+2]==null){
			data[i]=data[2*i+1];
		}else{
			data[i]=op.apply(data[2*i+1], data[2*i+2]);
		}
	}
	
	private void propagateModification(int i, int range){
		if(i<maxCap){
			long v = modification[i];
			modification[i]=null;
			data[2*i+1]+=modifyOp.apply((long) range/2, v);
			modification[2*i+1]=v;
			data[2*i+2]+=modifyOp.apply((long) range/2, v);
			modification[2*i+2]=v;
		}
	}

	public Long query(int left, int right) {
		if (left > right || left < 0 || right >= size)
			throw new IllegalArgumentException();
		return queryRec(0, 0, maxCap - 1, left, right);
	}

	private Long queryRec(int i, int li, int ri, int lo, int ro) {
		if(lo<=li && ri<= ro)
			return data[i];
		int mid = (ri+li)/2;
		if(modification[i]!=null){
			propagateModification(i, ri-li+1);
		}
		if(lo>mid)
			return queryRec(i*2+2, mid+1, ri, lo, ro);
		if(ro<=mid)
			return queryRec(i*2+1, li, mid, lo, ro);
		return op.apply(
				queryRec(i*2+2, mid+1, ri, lo, ro),
				queryRec(i*2+1, li, mid, lo, ro));
	}

	public int size() {
		return size;
	}

	@Override
	public String toString() {
		String str = "[";
		if (data[maxCap - 1] != null)
			str += data[maxCap - 1];
		for (int i = 1; i < size; i++) {
			str += ", " + data[maxCap - 1 + i];
		}
		return str + "]";
	}
}
