package rmq;

import java.util.function.BiFunction;

public class RMQ<T> {

	private int maxCap, size;
	private T[] data;
	private BiFunction<T, T, T> op;
	private static final int STARTCAP = 2;
	private boolean update=true;
	
	@SuppressWarnings("unchecked")
	public RMQ(BiFunction<T, T, T> op, T[] data){
		this.op=op;
		size= data.length;
		maxCap = STARTCAP;
		while(maxCap<data.length)
			maxCap*=2;
		this.data = (T[]) new Object[maxCap*2];
		for(int i=0; i<data.length; i++){
			this.data[maxCap-1+i]=data[i];
		}
		refresh();
	}
	
	@SuppressWarnings("unchecked")
	public RMQ(BiFunction<T, T, T> op){
		this.op=op;
		size=0;
		maxCap=STARTCAP;
		this.data = (T[]) new Object[maxCap*2];
	}
	
	@SuppressWarnings("unchecked")
	public RMQ(BiFunction<T, T, T> op, int initialCapacity){
		this.op=op;
		size=0;
		maxCap=STARTCAP;
		while(maxCap<initialCapacity)
			maxCap*=2;
		this.data = (T[]) new Object[maxCap*2];
	}
	
	public void efficientAdd(T elem){
		if(size==maxCap){
			addMaxCap();
		}
		data[maxCap-1+size]=elem;
		size++;
		update=false;
	}
	
	
	public void add(T elem){
		boolean aux=update;
		efficientAdd(elem);
		refresh(maxCap-2+size);
		update=aux;
	}
	
	public void efficientPut(int index, T elem){
		if (index>=size)
			throw new IllegalArgumentException();
		data[maxCap-1+index]=elem;
	}
	
	public void put(int index, T elem){
		boolean aux=update;
		efficientPut(index, elem);
		refresh(maxCap-1+index);
		update=aux;
	}
	
	@SuppressWarnings("unchecked")
	private void addMaxCap() {
		int newMaxCap = maxCap*2;
		T[] newData = (T[]) new Object[newMaxCap*2];
		for(int i=0; i<maxCap; i++){
			newData[newMaxCap-1+i]=data[maxCap-1+i];
		}
		maxCap=newMaxCap;
		data=newData;
		refresh();
		 
	}
	
	private void refresh(int i){
		while(i!=0){
			i=(i-1)/2;
			if(this.data[2*i+1]==null){
				this.data[i]=this.data[2*i+2];
				continue;
			}
			if(this.data[2*i+2]==null){
				this.data[i]=this.data[2*i+1];
				continue;
			}
			this.data[i]=op.apply(this.data[2*i+1], this.data[2*i+2]);
		}
	}

	private void refresh(){
		for(int i=maxCap-2; i>=0; i--){
			if(this.data[2*i+1]==null){
				this.data[i]=this.data[2*i+2];
				continue;
			}
			if(this.data[2*i+2]==null){
				this.data[i]=this.data[2*i+1];
				continue;
			}
			this.data[i]=op.apply(this.data[2*i+1], this.data[2*i+2]);
		}
		update=true;
	}
	
	public T query(int left, int right){
		if(left>right || left<0 || right>=size)
			throw new IllegalArgumentException();
		if (right==size-1)
			right=maxCap-1;
		if(!update)
			refresh();
		return queryRec(0, 0, maxCap-1, left, right);
	}
	
	
	private T queryRec(int i, int li, int ri, int lo, int ro) {
		if(lo<=li && ri<= ro)
			return data[i];
		int mid = (ri+li)/2;
		if(lo>mid)
			return queryRec(i*2+2, mid+1, ri, lo, ro);
		if(ro<=mid)
			return queryRec(i*2+1, li, mid, lo, ro);
		return op.apply(
				queryRec(i*2+2, mid+1, ri, lo, ro),
				queryRec(i*2+1, li, mid, lo, ro));
	}
	
	public void remove(int index){
		put(index, null);
	}
	
	public T get(int index){
		return data[maxCap-1+index];
	}

	public int size(){
		return size;
	}
	
	@Override
	public String toString() {
		String str = "[";
		if(data[maxCap-1]!=null) str+=data[maxCap-1];
		for(int i=1; i<size; i++){
			str+=", "+data[maxCap-1+i];
		}
		return str+"]";
	}
}
