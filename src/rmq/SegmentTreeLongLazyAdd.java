package rmq;

import java.util.function.BiFunction;

public class SegmentTreeLongLazyAdd {

	private int maxCap, size;
	private long[] data;
	private long[] modification;
	private BiFunction<Long, Long, Long> op;
	private BiFunction<Long, Long, Long> modifyOp;
	private static final int STARTCAP = 64;

	
	/**
	 * 
	 * @param data
	 * @param neutralElement: neutral of op
	 * @param op: (rangeValue, rangeValue) -> mergeRangeValue
	 * @param modifyOp: (range, value) -> modifyValue
	 */
	public SegmentTreeLongLazyAdd(long[] data, long neutralElement,
			BiFunction<Long, Long, Long> op,
			BiFunction<Long, Long, Long> modifyOp) {
		this.op = op;
		this.modifyOp = modifyOp;
		size = data.length;
		maxCap = STARTCAP;
		while (maxCap < data.length)
			maxCap *= 2;
		this.data = new long[maxCap * 2];
		this.modification = new long[maxCap * 2];
		for (int i = 0; i < data.length; i++) {
			this.data[maxCap - 1 + i] = data[i];
		}
		for (int i = maxCap - 1 + data.length; i < maxCap * 2; i++) {
			this.data[i] = neutralElement;
		}
		fillTree();
	}

	private void fillTree() {
		for (int i = maxCap - 2; i >= 0; i--) {
			this.data[i] = op.apply(this.data[2 * i + 1], this.data[2 * i + 2]);
		}
	}

	/**
	 * Adds value to the element in index.
	 */
	public void add(int index, long value) {
		addToRange(index, index, value);
	}

	/**
	 * Adds value to every element in [left, right].
	 */
	public void addToRange(int left, int right, long value) {
		addToRange(0, 0, maxCap - 1, left, right, value);
	}

	private void addToRange(int i, int li, int ri, int lo, int ro, long v) {
		if (lo <= li && ri <= ro) {
			data[i] += modifyOp.apply((long) (ri - li + 1), v);
			modification[i] += modification[i] + v;
			return;
		}
		if (modification[i] != 0) {
			propagateModification(i, ri - li + 1);
		}
		int mid = (ri + li) / 2;
		if (lo > mid) {
			addToRange(i * 2 + 2, mid + 1, ri, lo, ro, v);
		} else if (ro <= mid) {
			addToRange(i * 2 + 1, li, mid, lo, ro, v);
		} else {
			addToRange(i * 2 + 2, mid + 1, ri, lo, ro, v);
			addToRange(i * 2 + 1, li, mid, lo, ro, v);
		}
		data[i] = op.apply(data[2 * i + 1], data[2 * i + 2]);
	}

	private void propagateModification(int i, int range) {
		if (i < maxCap) {
			long v = modification[i];
			modification[i] = 0;
			data[2 * i + 1] += modifyOp.apply((long) range / 2, v);
			modification[2 * i + 1] = v;
			data[2 * i + 2] += modifyOp.apply((long) range / 2, v);
			modification[2 * i + 2] = v;
		}
	}

	public long get(int index){
		return query(index, index);
	}
	
	public long query(int left, int right) {
		if (left > right || left < 0 || right >= size)
			throw new IllegalArgumentException();
		return queryRec(0, 0, maxCap - 1, left, right);
	}

	private long queryRec(int i, int li, int ri, int lo, int ro) {
		if (lo <= li && ri <= ro)
			return data[i];
		if (modification[i] != 0) {
			propagateModification(i, ri - li + 1);
		}
		int mid = (ri + li) / 2;
		if (lo > mid)
			return queryRec(i * 2 + 2, mid + 1, ri, lo, ro);
		if (ro <= mid)
			return queryRec(i * 2 + 1, li, mid, lo, ro);
		return op.apply(queryRec(i * 2 + 2, mid + 1, ri, lo, ro),
				queryRec(i * 2 + 1, li, mid, lo, ro));
	}

	public int size() {
		return size;
	}

	@Override
	public String toString() {
		String str = "[";
		str += data[maxCap - 1];
		for (int i = 1; i < size; i++) {
			str += ", " + data[maxCap - 1 + i];
		}
		return str + "]";
	}
}
