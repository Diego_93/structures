package trie;

import java.util.HashMap;
import java.util.Map;

public class Trie {
	
	private Node root;
	
	public void add(String str){
		add(root, str, 0);
	}
	
	private void add(Node currentNode, String str, int index){
		if(index==str.length()){
			currentNode.exist=true;
			return;
		}
		char letter = str.charAt(index);
		if(currentNode.next.get(letter)==null)
			currentNode.next.put(letter, new Node());
		add(currentNode.next.get(letter), str, index+1);
	}
	
	public boolean contains(String str){
		return contains(root, str, 0);
	}
	
	private boolean contains(Node currentNode, String str, int index){
		if(currentNode==null)
			return false;
		if(index==str.length())
			return currentNode.exist;
		char letter = str.charAt(index);
		return contains(currentNode.next.get(letter),str, index+1);
	}
	
	public void remove(String str){
		remove(root, str, 0);
	}
	
	public void remove(Node currentNode, String str, int index){
		if(currentNode==null)
			return;
		if(index==str.length())
			currentNode.exist=false;
		char letter = str.charAt(index);
		remove(currentNode.next.get(letter),str, index+1);
	}
	
	private class Node{
		boolean exist;
		Map<Character,Node> next;
		
		Node(){
			next = new HashMap<Character, Node>();
		}
	}
}
