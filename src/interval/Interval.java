package interval;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


@SuppressWarnings("unused")
public class Interval<T extends Comparable<T>> {
	
	private Set<MyInterval<T>> intervals;
	
	
	/**
	 * 	Create a empty set of intervals.
	 *  The null values on the bounds of the intervals represent an infinit bound.
	 */
	public Interval() {
		intervals=new TreeSet<MyInterval<T>>(new Comparator<MyInterval<T>>() {
			public int compare(MyInterval<T> o1, MyInterval<T> o2) {
				if(o1.left==null)
					return -1;
				if(o2.left==null)
					return 1;
				return o1.left.compareTo(o2.left);
			}
		});
	}
	
	/**
	 * Joins two specified sets of intervals.
	 * 
	 * @param interval1 - set of interval to be joined.
	 * @param interval2 - set of interval to be joined.
	 * @return the set of intervals that contains interval1 and interval2.
	 */
	public static<T extends Comparable<T>> Interval<T> union(Interval<T> interval1, Interval<T> interval2){
		Interval<T> ans = new Interval<T>();
		
		for (MyInterval<T> myInterval : interval1.intervals) {
			ans.add(myInterval);
		}
		for (MyInterval<T> myInterval : interval2.intervals) {
			ans.add(myInterval);
		}
		return ans;
	}
	
	
	/**
	 * Add the interval [left, right] to the actual set of intervals.
	 * If left or right are null, the bound will be not inclusive. (e.g. (null,6] )
	 * 
	 * @param left - left bound of the interval to be added.
	 * @param right - right bound of the interval to be added
	 * 
	 * @throws IllegalStateException if left is greater than right.
	 */
	public void add(T left, T right){
		add(left,right,left==null?false:true,right==null?false:true);
	}
	
	private void add(MyInterval<T> myInterval){
		add(myInterval.left,myInterval.right,myInterval.leftInclusive,myInterval.rightInclusive);
	}
	
	
	/**
	 * Add the interval [left, right] to the actual set of intervals.
	 * The inclusion of the bound can be modified by the arguments li and ri.
	 * 
	 * @param left - left bound of the interval to be added.
	 * @param right - right bound of the interval to be added
	 * @param li - inclusion of the left bound (if it's true is inclusive)
	 * @param ri - inclusion of the right bound (if it's true is inclusive)
	 * 
	 * @throws IllegalStateException if left is greater than right, or the bound is inclusive when the value of it is null,
	 * or left and right are equals and some bound it's not inclusive.
	 */
	public void add(T left, T right, boolean li, boolean ri){
		MyInterval<T> interval = new MyInterval<T>(left, right, li, ri);
		Iterator<MyInterval<T>> it = intervals.iterator();
		boolean hadRemove=false;
		while(it.hasNext()){
			MyInterval<T> actual=it.next();
			MyInterval<T> aux=MyInterval.union(interval, actual);
		//	System.out.println(actual+" "+interval+" "+aux);
			if(aux!=null){
				interval=aux;
				it.remove();
				hadRemove=true;
			}else{
				if(hadRemove)
					break;
			}		
		}
		intervals.add(interval);
		return;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((intervals == null) ? 0 : intervals.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		if(intervals.isEmpty())
			return "{}";
		StringBuffer aux=new StringBuffer();
		for (MyInterval<T> myInterval : intervals) {
			aux.append(myInterval.toString()+"U");
		}
		aux.setLength(aux.length()-1);
		return aux.toString();
	}
	
	/**
	 * Returns if this set of intervals contains the specified element.
	 * If the specified element is null, returns false.
	 * 
	 * @param n - element whose presence in this set of intervals is to be tested.
	 *   
	 * @return true if this set of intervals contains the specified element
	 * 
	 */
	public boolean contains(T n){
		if(n==null)
			return false;
		for (MyInterval<T> myInterval : intervals) {
			if(myInterval.contains(n))
				return true;
			if(myInterval.left!=null && myInterval.left.compareTo(n)>0)
				return false;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interval other = (Interval) obj;
		if (intervals == null) {
			if (other.intervals != null)
				return false;
		} else if (!intervals.equals(other.intervals))
			return false;
		return true;
	}
	
	
	/**
	 * Complements an specified set of intervals.
	 * 
	 * @param interval - set of interval to be complemented.
	 * 
	 * @return the set of intervals which is the complement of interval.
	 */
	public static<T extends Comparable<T>> Interval<T> complement(Interval<T> interval){
		Interval<T> answer= new Interval<T>();
		T left=null;
		boolean li=false;
		for (MyInterval<T> t : interval.intervals) {
			if(left!=null || t.left!=null){
				answer.add(left,t.left,li,!t.leftInclusive);
			}
			left=t.right;
			li=!t.rightInclusive;
		}
		if(left!=null){
			answer.add(left,null,li,false);
		}
		return answer;
	}
	
	
	/**
	 * Intersects two specified sets of intervals.
	 * 
	 * @param interval1 - set of interval to be intersect.
	 * @param interval2 - set of interval to be intersect.
	 * @return the set of intervals that contains the intersection between interval1 and interval2.
	 */
	public static<T extends Comparable<T>> Interval<T> intersection(Interval<T> interval1, Interval<T> interval2){
		Interval<T> ans = new Interval<T>();
		for (MyInterval<T> t1 : interval1.intervals) {
			for (MyInterval<T> t2 : interval2.intervals) {
				MyInterval<T> aux=MyInterval.intersection(t1, t2);
				if(aux!=null)
					ans.add(aux);
			}
		}
		return ans;
	}
	
	
	public static<T extends Comparable<T>> Interval<T> symmetricDifference(Interval<T> interval1, Interval<T> interval2){
		return difference(union(interval1, interval2), intersection(interval1, interval2));
	}
	
	public static<T extends Comparable<T>> Interval<T> difference(Interval<T> interval1, Interval<T> interval2){
		return intersection(interval1, complement(interval2));
	}
	
	private static class MyInterval<T extends Comparable<T>>{
		
		T left, right;
		boolean leftInclusive, rightInclusive;
		
		public MyInterval(T l, T r){
			this(l,r,true,true);
		}
		
		public MyInterval(T l, T r, boolean li, boolean ri){
			isValid(l,r,li,ri);
			this.left=l;
			this.right=r;
			leftInclusive=li;
			rightInclusive=ri;
		}
				
		public boolean isLeftInclusive() {
			return leftInclusive;
		}

		public boolean isRightInclusive() {
			return rightInclusive;
		}

		public void setLeftInclusive(boolean li) {
			isValid(left,right,li,rightInclusive);
			this.leftInclusive = li;
		}

		public void setRightInclusive(boolean ri) {
			isValid(left,right,leftInclusive,ri);
			this.rightInclusive = ri;
		}

		public boolean contains(T n){
			if(n==null)
				return false;
			if((left==null || left.compareTo(n)<0 || (leftInclusive && left==n)) && (right==null || n.compareTo(right)<0 || (rightInclusive && right==n)))
				return true;
			return false;
		}
		
		public boolean isValid(T l, T r, boolean li, boolean ri){
			if(l!=null && r!=null && (r.compareTo(l)<0 || (l.compareTo(r)==0 && (!li || !ri))))
				throw new IllegalStateException();
			if((l==null && li) || (r==null && ri))
				throw new IllegalStateException();
			return true;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj==null)
				return false;
			if(!(obj instanceof MyInterval))
				return false;
			MyInterval<T> o = (MyInterval<T>) obj;
			if(o.left!=null && !o.left.equals(left))
				return false;
			if(left==null)
				return false;
			if(o.right!=null && !o.right.equals(right))
				return false;
			if(right==null)
				return false;
			if(rightInclusive!=o.rightInclusive || leftInclusive!=o.leftInclusive)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return (leftInclusive?"[":"(")+(left==null?"-INF":left)+","+(right==null?"+INF":right)+(rightInclusive?"]":")");
		}
		
		public static<T extends Comparable<T>> boolean disjoint(MyInterval<T> a, MyInterval<T> b){
			T left=a.left;
			T right=a.right;
			boolean leftInclusive=a.leftInclusive;
			boolean rightInclusive=a.rightInclusive;
			if(left==null){
				if(right==null || b.left==null || right.compareTo(b.left)>0 || (right.compareTo(b.left)==0 && rightInclusive && b.leftInclusive))
					return false;
				return true;
			}
			if(right==null){
				if(b.right==null || left.compareTo(b.right)<0 || (left.compareTo(b.right)==0 && leftInclusive && b.rightInclusive))
					return false;
				return true;
			}
			if(b.left!=null){
				if(b.left.compareTo(right)>0 || (right.compareTo(b.left)==0 && (!rightInclusive || !b.leftInclusive)))
					return true;
				if(b.right!=null && (left.compareTo(b.right)>0 || (left.compareTo(b.right)==0 && (!leftInclusive || !b.rightInclusive))))
					return true;
				return false;
			}
			if(b.right!=null){
				if(b.right.compareTo(left)<0 || (left.compareTo(b.right)==0 && (!leftInclusive || !b.rightInclusive)))
					return true;
				return false;
			}
			return false;
		}
		
		public static<T extends Comparable<T>> MyInterval<T> intersection(MyInterval<T> a, MyInterval<T> b){
			if(disjoint(a, b))
				return null;
			T left,right;
			boolean li,ri;
			
			if(a.left==null){
				left=b.left;
				li=b.leftInclusive;
			}else{
				if(b.left==null){
					left=a.left;
					li=a.leftInclusive;
				}else{
					if(a.left.compareTo(b.left)==0){
						left=a.left;
						li=a.leftInclusive&b.leftInclusive;
					}else
					if(a.left.compareTo(b.left)<0){
						left=b.left;
						li=b.leftInclusive;
					}else{
						left=a.left;
						li=a.leftInclusive;
					}
				}
			}
			
			if(a.right==null){
				right=b.right;
				ri=b.rightInclusive;
			}else{
				if(b.right==null){
					right=a.right;
					ri=a.rightInclusive;
				}else{
					if(a.right.compareTo(b.right)==0){
						right=a.right;
						ri=a.rightInclusive&b.rightInclusive;
					}else
					if(a.right.compareTo(b.right)>0){
						right=b.right;
						ri=b.rightInclusive;
					}else{
						right=a.right;
						ri=a.rightInclusive;
					}
				}
			}
			
			return new MyInterval<T>(left, right, li, ri);
		}
		
		public static<T extends Comparable<T>> MyInterval<T> union(MyInterval<T> a, MyInterval<T> b){
			if(disjoint(a, b)){
				if(a.left!=null && b.right!=null && (a.left.compareTo(b.right)!=0) || (!a.leftInclusive && !b.rightInclusive))
					return null;
				if(b.left!=null && a.right!=null && (b.left.compareTo(a.right)!=0) || (!b.leftInclusive && !a.rightInclusive))
					return null;
			}
			T left=null, right=null;
			boolean li=false, ri=false;
			if(a.left!=null && b.left!=null){
				if(a.left.compareTo(b.left)==0){
					left=a.left;
					li=a.leftInclusive|b.leftInclusive;
				}else{
					if(a.left.compareTo(b.left)<0){
						left=a.left;
						li=a.leftInclusive;
					}else{
						left=b.left;
						li=b.leftInclusive;
					}
				}
			}
			if(a.right!=null && b.right!=null){
				if(a.right.compareTo(b.right)==0){
					right=a.right;
					ri=a.rightInclusive|b.rightInclusive;
				}else{
					if(a.right.compareTo(b.right)>0){
						right=a.right;
						ri=a.rightInclusive;
					}else{
						right=b.right;
						ri=b.rightInclusive;
					}
				}
			}
			return new MyInterval<T>(left, right, li, ri);
		}
	}
}
