package interval;


public class IntervalSamples {

	public static void main(String[] args) {
		Interval<Integer> a=new Interval<Integer>(), b= new Interval<Integer>();
		a.add(2, 5, false, true);
		a.add(6, 8);
		b.add(3,7);
		b.add(20,null);

		System.out.println("a: "+a);
		System.out.println("b: "+b);
		Interval<Integer> c=Interval.union(a, b);

		System.out.println("aUb: "+c);
		Interval<Integer> d=Interval.intersection(a, b);

		System.out.println("a^b: "+d);
		Interval<Integer> e=Interval.complement(a);

		System.out.println("!a: "+e);
		Interval<Integer> f=Interval.complement(b);

		System.out.println("!b: "+f);
		Interval<Integer> g=Interval.difference(b, a);

		System.out.println("b-a: "+g);
		Interval<Integer> h=Interval.difference(a, b);
		System.out.println("a-b: "+h);
		Interval<Integer> i=Interval.symmetricDifference(a, b);
		System.out.println("a~b: "+i);
	}
}