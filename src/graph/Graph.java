package graph;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class Graph<V> {

	List<V> verteces;
	Map<V, List<Edge>> neighbours;
	
	public Graph() {
		neighbours = new HashMap<V, List<Edge>>();
		verteces = new LinkedList<V>();
	}

	public boolean addVertex(V vertex) {
		if(verteces.contains(vertex))
			return false;
		verteces.add(vertex);
		neighbours.put(vertex, new LinkedList<Edge>());
		return true;
	}

	public void addEdge(V v1, V v2, int weight) {
		neighbours.get(v1).add(new Edge(v2, weight));
		neighbours.get(v2).add(new Edge(v1, weight));
	}
	
	public Map<V, Long> dijkstraFrom(V vInit) {
		Map<V, Long> result = new HashMap<V, Long>();
		Map<V, Long> preliminary = new HashMap<V, Long>();
		PQNode node = new PQNode(vInit, 0);
		PriorityQueue<PQNode> heap = new PriorityQueue<PQNode>(
				new Comparator<PQNode>() {
					public int compare(PQNode o1, PQNode o2) {
						if(o1.weight==o2.weight)
							return 0;
						return (o1.weight>o2.weight?1:-1);
					}
				});
		heap.add(node);
		preliminary.put(vInit, 0L);
		while(!heap.isEmpty()){
			node = heap.poll();
			if(result.get(node.vertex)==null){
				result.put(node.vertex, node.weight);
				for(Edge e : neighbours.get(node.vertex)){
					Long preliminaryDistance = preliminary.get(e.vertex);
					if(result.get(e.vertex)==null && (preliminaryDistance==null || preliminaryDistance>node.weight+e.weight)){
						preliminary.put(e.vertex, node.weight+e.weight);
						heap.add(new PQNode(e.vertex, node.weight+e.weight));
					}
				}
			}
		}
		return result;
	}
	
	public Long dijkstra(V vInit, V vFinal) {
		Map<V, Long> result = new HashMap<V, Long>();
		Map<V, Long> preliminary = new HashMap<V, Long>();
		PQNode node = new PQNode(vInit, 0);
		PriorityQueue<PQNode> heap = new PriorityQueue<PQNode>(
				new Comparator<PQNode>() {
					public int compare(PQNode o1, PQNode o2) {
						if(o1.weight==o2.weight)
							return 0;
						return (o1.weight>o2.weight?1:-1);
					}
				});
		heap.add(node);
		preliminary.put(vInit, 0L);
		while(!heap.isEmpty()){
			node = heap.poll();
			if(result.get(node.vertex)==null){
				if(node.vertex.equals(vFinal))
					return node.weight;
				result.put(node.vertex, node.weight);
				for(Edge e : neighbours.get(node.vertex)){
					Long preliminaryDistance = preliminary.get(e.vertex);
					if(result.get(e.vertex)==null && (preliminaryDistance==null || preliminaryDistance>node.weight+e.weight)){
						preliminary.put(e.vertex, node.weight+e.weight);
						heap.add(new PQNode(e.vertex, node.weight+e.weight));
					}
				}
			}
		}
		return null;
	}
	
	/*public Long djkstra(V vInit, V vFinal) {
		Map<V, Long> result = new HashMap<V, Long>();
		Node node = new Node(vInit, 0);
		PriorityQueue<Node> heap = new PriorityQueue<Node>(
				new Comparator<Node>() {
					@Override
					public int compare(Node o1, Node o2) {
						if(o1.weight==o2.weight)
							return 0;
						return (o1.weight>o2.weight?1:-1);
					}
				});
		heap.add(node);
		while(!heap.isEmpty()){
			node = heap.poll();
			if(result.get(node.vertex)==null){
				if(node.vertex.equals(vFinal))
					return node.weight;
				result.put(node.vertex, node.weight);
				for(Edge e : neighbours.get(node.vertex)){
					if(result.get(e.vertex)==null || result.get(e.vertex)>node.weight+e.weight){
						heap.add(new Node(e.vertex, node.weight+e.weight));
					}
				}
			}
		}
		return null; // No connected
	}*/

	private class PQNode {
		V vertex;
		long weight;

		public PQNode(V vertex, long weight) {
			this.vertex = vertex;
			this.weight = weight;
		}
	}
	
	private class Edge{
		V vertex;
		long weight;
		
		public Edge(V vertex, long weight) {
			this.vertex = vertex;
			this.weight = weight;
		}
	}
}
