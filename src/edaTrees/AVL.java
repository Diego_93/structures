package edaTrees;

import java.util.Comparator;

public class AVL<T> extends BST<T> {

	Node<T> root;
	
	public AVL(Comparator<T> cmp) {
		super(cmp);
	}

	protected Node<T> add(Node<T> root,T key){
		if (root==null){
			size++;
			cantOperations++;
			return new Node<T>(key);
		}
		if (cmp.compare(root.value, key)>0){
			root.left=add(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			root.right=add(root.right,key);
		}
		return balanceTree(root);		
	}
	
	protected Node<T> remove(Node<T> root, T key){
		if (root==null){
			return null;
		}
		if (root.value.equals(key)){
			size--;
			cantOperations++;
			if(root.left==null){
				return root.right;
			}
			root.left=cutAndPasteMax(root.left,root);
		}
		if (cmp.compare(root.value, key)>0){
			root.left=remove(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			root.right=remove(root.right,key);
		}
		return balanceTree(root);
	}
	
	protected Node<T> cutAndPasteMax(Node<T> root, Node<T> to){
		if(root.right==null){
			to.value=root.value;
			return root.left;
		}
		root.right=cutAndPasteMax(root.right, to);
		return balanceTree(root);
	}
	
	
	protected static <T> Node<T> balanceTree(Node<T> root){
		if(height(root.left)-height(root.right)>1){
			if(height(root.left.left)-height(root.left.right)>=0){
				root=rotateRight(root);
			}else{
				root.left=rotateLeft(root.left);
				root=rotateRight(root);
			}
		}else
		if(height(root.left)-height(root.right)<-1){
			if(height(root.right.left)-height(root.right.right)<=0){
				root=rotateLeft(root);
			}else{
				root.right=rotateRight(root.right);
				root=rotateLeft(root);
			}
		}
		calculateHeight(root);
		return root;
	}
	
	
	protected static <T> Node<T> rotateRight(Node<T> root){
		Node<T> aux=root.left;
		root.left=root.left.right;
		aux.right=root;
		calculateHeight(aux);
		calculateHeight(aux.right);
		return aux;
	}
	
	protected static <T> Node<T> rotateLeft(Node<T> root){
		Node<T> aux=root.right;
		root.right=root.right.left;
		aux.left=root;
		calculateHeight(aux);
		calculateHeight(aux.left);
		return aux;
	}
	
	protected static <T> void calculateHeight(Node<T> root){
		root.height=Math.max(height(root.left), height(root.right))+1;
	}
	
	protected static <T> int height(Node<T> root){
		if(root==null){
			return -1;
		}
		return root.height;
	}
}
