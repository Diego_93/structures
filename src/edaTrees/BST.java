package edaTrees;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


public class BST<T> extends BinaryTree<T> implements BinarySearchTree<T> {

	protected int size=0;
	protected Comparator<? super T> cmp;
	protected int cantOperations;
	
	public BST(Comparator<T> cmp){
		super();
		this.cmp=cmp;
	}
	
	public BST(Node<T> root,Comparator<T> cmp){
		super(root);
		this.cmp=cmp;
	}
	
	public BST(Node<T> root,Comparator<T> cmp,int size){
		super(root);
		this.cmp=cmp;
		this.size=size;
	}
	
	@Override
	public void add(T key) {
		root=add(root,key);
	}
	
	protected Node<T> add(Node<T> root,T key){
		if (root==null){
			size++;
			cantOperations++;
			return new Node<T>(key);
		}
		if (cmp.compare(root.value, key)>0){
			root.left=add(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			root.right=add(root.right,key);
		}
		return root;		
	}

	@Override
	public void remove(T key) {
		root=remove(root,key);
	}
	
	protected Node<T> remove(Node<T> root, T key){
		if (root==null){
			return null;
		}
		if (root.value.equals(key)){
			size--;
			cantOperations++;
			if(root.left==null){
				return root.right;
			}
			root.left=cutAndPasteMax(root.left,root);
		}
		if (cmp.compare(root.value, key)>0){
			root.left=remove(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			root.right=remove(root.right,key);
		}
		return root;
	}
	
	protected Node<T> cutAndPasteMax(Node<T> root, Node<T> to){
		if(root.right==null){
			to.value=root.value;
			return root.left;
		}
		root.right=cutAndPasteMax(root.right, to);
		return root;
	}

	@Override
	public boolean contains(T key) {
		return contains(root,key);
	}
	
	protected boolean contains(Node<T> root, T key){
		if(root==null){
			return false;
		}
		if (cmp.compare(root.value, key)>0){
			return contains(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			return contains(root.right,key);
		}
		return true;
	}

	@Override
	public int size() {
		return size;
	}

	public static BST<Integer> toBST(int[] arr){
		return new BST<Integer>(toBST(arr,0,arr.length),new Comparator<Integer>(){
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		},arr.length);
	}
	
	protected static Node<Integer> toBST(int[] arr, int left, int right){
		if (left==right)
			return null;
		int mid=(left+right)/2;
		return new Node<Integer>(arr[mid],toBST(arr, left, mid),toBST(arr, mid+1, right));
	}

	@Override
	public int levelOf(T key) {
		return levelOf(root,key);
	}
	
	protected int levelOf(Node<T> root,T key){
		if(root==null)
			return -1;
		if(root.value.equals(key)){
			return 0;
		}
		int aux=-1;
		if (cmp.compare(root.value, key)>0){
			aux=levelOf(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			aux=levelOf(root.right,key);
		}
		return (aux==-1?aux:aux+1);
	}

	@Override
	public int leaves() {
		return leaves(root);
	}
	
	protected int leaves(Node<T> root){
		if(root==null)
			return 0;
		if(root.left==null && root.right==null)
			return 1;
		return leaves(root.left)+leaves(root.right);
	}

	@Override
	public T max() {
		if (root==null)
			return null;
		Node<T> aux=root;
		while(aux.right!=null){
			aux=aux.right;
		}
		return aux.value;
	}
	

	@Override
	public void printAncestorsOf(T key) {
		printAncestorsOf(root,key);
	}
	
	protected boolean printAncestorsOf(Node<T> root, T key){
		if(root==null)
			return false;
		if(root.value.equals(key)){
			return true;
		}
		boolean aux=false;
		if (cmp.compare(root.value, key)>0){
			aux=printAncestorsOf(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			aux=printAncestorsOf(root.right,key);
		}
		if (aux){
			System.out.println(root.value);
		}
		return aux;
	}

	@Override
	public void printPredecessorsOf(T key) {
		printPredecessorsOf(root, key);
	}
	
	protected void printPredecessorsOf(Node<T> root, T key){
		if(root==null)
			return;
		if(root.value.equals(key)){
			print(root.left);
			print(root.right);
			return;
		}
		if (cmp.compare(root.value, key)>0){
			printPredecessorsOf(root.left,key);
		}else if (cmp.compare(root.value, key)<0){
			printPredecessorsOf(root.right,key);
		}
		return;
	}
	
	protected void print(Node<T> root){
		if(root==null)
			return;
		System.out.println(root.value);
		print(root.left);
		print(root.right);
	}
	
	public boolean equals(Object o){
		if(o==null)
			return false;
		if(o==this)
			return true;
		if(o instanceof BinaryTree){
			return equals((BinaryTree) o);
		}
		return false;
	}
	
	public boolean equals(BinaryTree<T> tree){
		return equals(root,tree.root);
	}
	
	protected boolean equals(Node<T> r1, Node<T> r2){		
		if(r1==null && r2==null)
			return true;
		if(r1==null || r2==null || !r1.value.equals(r2.value))
			return false;
		return equals(r1.left,r2.left)&&equals(r1.right, r2.right);
	}
	
	public int hashCode(){
		if (root==null)
			return 1;
		return(7*size+11*root.value.hashCode()+17*(root.left==null?0:root.left.hashCode())+19*17*(root.right==null?0:root.right.hashCode()));
	}
	
	
	private class myIterator<T> implements Iterator<T>{

			int cantOp=cantOperations;
			Iterator<T> itt;
			
			public void selectOrder(ArrayList<T> arr){
				itt=arr.iterator();
			}
			
			@Override
			public boolean hasNext() {
				if (cantOp!=cantOperations)
					throw new IllegalStateException();
				return itt.hasNext();
			}
	
			@Override
			public T next() {
				if (cantOp!=cantOperations)
					throw new IllegalStateException();
				return itt.next();
			}
	
			@Override
			public void remove() {
				if (cantOp!=cantOperations)
					throw new IllegalStateException();
				itt.remove();
			}
	}
	
	public Iterator<T> preOrder(){
		myIterator<T> it= new myIterator<T>();
		ArrayList<T> arr=new ArrayList<T>();
		toPreOrder(root, arr);		
		it.selectOrder(arr);
		return it;
	}
	
	public Iterator<T> inOrder(){
		myIterator<T> it= new myIterator<T>();
		ArrayList<T> arr=new ArrayList<T>();
		toInOrder(root, arr);		
		it.selectOrder(arr);
		return it;
	}
	
	public Iterator<T> postOrder(){
		myIterator<T> it= new myIterator<T>();
		ArrayList<T> arr=new ArrayList<T>();
		toPostOrder(root, arr);		
		it.selectOrder(arr);
		return it;
	}
	
	protected void toPreOrder(Node<T> root, List<T> lista){
		if(root==null)	return;
		
		lista.add(root.value);
		toPreOrder(root.left, lista);
		toPreOrder(root.right, lista);
	}
	
	protected void toInOrder(Node<T> root, List<T> lista){
		if(root==null)	return;
		
		toInOrder(root.left, lista);
		lista.add(root.value);
		toInOrder(root.right, lista);
	}
	
	protected void toPostOrder(Node<T> root, List<T> lista){
		if(root==null)	return;
		
		toPostOrder(root.left, lista);
		toPostOrder(root.right, lista);
		lista.add(root.value);
	}
	
	public ArrayList<T> elementsInRange(T min, T max){
		ArrayList<T> ans= new ArrayList<T>();
		if (min==null || max==null || (cmp.compare(max, min)<0)){
			throw new InvalidParameterException();
		}
		elementsInRange(root,min,max,ans);
		return ans;
	}
	
	protected void elementsInRange(Node<T> root,T min, T max, ArrayList<T> list){
		if(root==null)
			return;
		if (cmp.compare(min, root.value)<0 && cmp.compare(root.value, max)<0){
			list.add(root.value);
		}
		if (cmp.compare(min, root.value)<0){
			elementsInRange(root.left, min, max, list);
		}
		if (cmp.compare(max, root.value)>0){
			elementsInRange(root.right, min, max, list);
		}
	}
}
