package edaTrees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTree<T> {

	Node<T> root;
	
	public BinaryTree(){
		root=null;
	}
	
	public BinaryTree(Node<T> root){
		this.root=root;
	}
	
	protected static class Node<T>{
		T value;
		Node<T> left,right;
		int height;
		
		public Node(){
			this(null,null,null);
		}
		
		public Node(T value){
			this(value,null,null);
		}
		
		
		public Node(T value, Node<T> left, Node<T> right){
			this.value=value;
			this.left=left;
			this.right=right;
		}
	}
	
	public static <T,S> BinaryTree<S> transform(BinaryTree<T> tree, Transform<T, S> fn){
		return new BinaryTree<S>(transform(tree.root,fn));
	}
	
	protected static <T,S> Node<S> transform(Node<T> root, Transform<T, S> fn){
		if (root==null){
			return null;
		}
		return new Node<S>(fn.transform(root.value),transform(root.left, fn),transform(root.right, fn));		
	}
	
	public static <T> boolean areMirror(BinaryTree<T> t1, BinaryTree<T> t2){
		return areMirror(t1.root,t2.root);
	}
	
	protected static <T> boolean areMirror(Node<T> t1, Node<T> t2){
		if(t1==null && t2==null)
			return true;
		if(t1==null || t2==null)
			return false;
		return areMirror(t1.left, t2.right) && areMirror(t1.right,t2.left);
	}
	
	
	
	public void printByLevel(){
		
		Queue<Node<T>> cola = new LinkedList<Node<T>>();
		cola.add(root);
		while(!cola.isEmpty()){
			Node<T> aux=cola.remove();
			System.out.println(aux.value);
			if (aux.left!=null) cola.add(aux.left);
			if (aux.right!=null) cola.add(aux.right);
		}
	}
		
	protected void printByLevel(Node<T> root){
		Queue<Node<T>> cola = new LinkedList<Node<T>>();
		cola.add(root);
		while(!cola.isEmpty()){
			Node<T> aux=cola.remove();
			System.out.println(aux.value);
			if (aux.left!=null) cola.add(aux.left);
			if (aux.right!=null) cola.add(aux.right);
		}
	}
	
	
	public boolean isBST(Comparator<T> cmp){
		return isBST(root,cmp,null,null);		
	};
	
	protected boolean isBST(Node<T> root,Comparator<T> cmp,T min,T max){
		if(root==null){
			return true;
		}
		if (min!=null && cmp.compare(min, root.value)>0){
			return false;
		}
		if (max!=null && cmp.compare(max, root.value)<0){
			return false;
		}
		return isBST(root.left,cmp,min,root.value)&&isBST(root.right,cmp,root.value,max);
	}
	
	public boolean isAVL(){
		return (isAVL(root)!=-2);
	};
	
	private int isAVL(Node<T> root){
		if(root==null){
			return -1;
		}
		int left,right;
		if ((left=isAVL(root.left))==-2 || (right=isAVL(root.right))==-2 || Math.abs(right-left)>1){
			return -2;
		}
		return Math.max(left, right)+1;	
	}
	
	
	
	/*--------------------------------------------------------------------------------------*/
	
	protected static <T> void printNode(Node<T> root) {
		int maxLevel = BinaryTree.maxLevel(root);
		printNodeInternal(Collections.singletonList(root), 1, maxLevel);
	}

	private static <T> void printNodeInternal(List<Node<T>> nodes,	int level, int maxLevel) {
		if (nodes.isEmpty() || BinaryTree.isAllElementsNull(nodes))
			return;
		int floor = maxLevel - level;
		int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
		int firstSpaces = (int) Math.pow(2, (floor)) - 1;
		int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;
		BinaryTree.printWhitespaces(firstSpaces);
		List<Node<T>> newNodes = new ArrayList<Node<T>>();
		for (Node<T> node : nodes) {
			if (node != null) {
				System.out.print(node.value);
				newNodes.add(node.left);
				newNodes.add(node.right);
			} else {
				newNodes.add(null);
				newNodes.add(null);
				System.out.print(" ");
			}
			BinaryTree.printWhitespaces(betweenSpaces);
		}
		System.out.println("");
		for (int i = 1; i <= endgeLines; i++) {
			for (int j = 0; j < nodes.size(); j++) {
				BinaryTree.printWhitespaces(firstSpaces - i);
				if (nodes.get(j) == null) {
					BinaryTree.printWhitespaces(endgeLines + endgeLines	+ i + 1);
					continue;
				}
				if (nodes.get(j).left != null)
					System.out.print("/");
				else
					BinaryTree.printWhitespaces(1);
				BinaryTree.printWhitespaces(i + i - 1);
				if (nodes.get(j).right != null)
					System.out.print("\\");
				else
					BinaryTree.printWhitespaces(1);
				BinaryTree.printWhitespaces(endgeLines + endgeLines - i);
			}
			System.out.println("");
		}
		printNodeInternal(newNodes, level + 1, maxLevel);
	}

	private static void printWhitespaces(int count) {
		for (int i = 0; i < count; i++)
			System.out.print(" ");
	}

	private static <T> int maxLevel(Node<T> node) {
		if (node == null)
			return 0;
		return Math.max(BinaryTree.maxLevel(node.left),
		BinaryTree.maxLevel(node.right)) + 1;
	}

	private static <T> boolean isAllElementsNull(List<T> list) {
		for (Object object : list) {
			if (object != null)
				return false;
		}
		return true;
	}
	
	public void graph(){
		printNode(root);
	}
}
