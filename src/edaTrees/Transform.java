package edaTrees;

public interface Transform<T,S> {

	public S transform(T value); 
}
