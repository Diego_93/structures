import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class Solution {

	static int n,m;
	static int[] sock, comp;
	
	public void solve() {
		n = ni();
		m = ni();
		TreeSet<Point> c = new TreeSet<Point>();
		TreeSet<Point> s = new TreeSet<Point>();
		comp = new int[n];
		sock = new int[m];
		 // O(n lg n)
		for(int i=0; i<n; i++){
			c.add(new Point(ni(), i));
		}
		 // O(m lg m)
		for(int i=0; i<m; i++){
			s.add(new Point(ni(), i));
		}
		int ans=0;
		int totalAdap =0;
		List<Point> aux = new LinkedList<Point>();
		for(Point ss : s){
			aux.add(ss);
		}
		for(Point ss : aux){
			int x = ss.x;
			int adap = 0;
			//O(lg(10^9))*lg(n)
			do{
				Point cc = c.floor(new Point(x+1,-1));
				if(cc==null){
					break;
				}
				if(cc.x == x){
					comp[cc.y] = ss.y+1;
					sock[ss.y] = adap;
					ans++;
					totalAdap+=adap;
					c.remove(cc);
					break;
				}else{
					do{
						adap++;
						x=(x/2)+(x%2);
					}while(cc.x<x);
				}
			}while(x>0);
		}
		StringBuilder str = new StringBuilder();
		str.append(ans+" "+totalAdap+"\n");
		for(int i=0; i<m-1; i++){
			str.append(sock[i]+" ");
		}
		str.append(sock[m-1]+"\n");
		for(int i=0; i<n-1; i++){
			str.append(comp[i]+" ");
		}
		str.append(comp[n-1]+"\n");
		System.out.println(str.toString());
	}
	
	public static class Point implements Comparable<Point>{
		int x, y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int compareTo(Point o) {
			Integer a = Integer.compare(x, o.x);
			if(a==0)
				return Integer.compare(y, o.y);
			return a;
		}
	}
	
	InputStream is;
	PrintWriter out;
	String INPUT = "";
	
	void run() throws Exception
	{
		is = INPUT.isEmpty() ? System.in : new ByteArrayInputStream(INPUT.getBytes());
		out = new PrintWriter(System.out);
		long s = System.currentTimeMillis();
		solve();
		out.flush();
		if(!INPUT.isEmpty())tr(System.currentTimeMillis()-s+"ms");
	}
	
	public static void main(String[] args) throws Exception { new Solution().run(); }
	
	private byte[] inbuf = new byte[1024];
	private int lenbuf = 0, ptrbuf = 0;
	
	private int readByte()
	{
		if(lenbuf == -1)throw new InputMismatchException();
		if(ptrbuf >= lenbuf){
			ptrbuf = 0;
			try { lenbuf = is.read(inbuf); } catch (IOException e) { throw new InputMismatchException(); }
			if(lenbuf <= 0)return -1;
		}
		return inbuf[ptrbuf++];
	}
	
	private boolean isSpaceChar(int c) { return !(c >= 33 && c <= 126); }
	private int skip() { int b; while((b = readByte()) != -1 && isSpaceChar(b)); return b; }
	
	private double nd() { return Double.parseDouble(ns()); }
	private char nc() { return (char)skip(); }
	
	private String ns()
	{
		int b = skip();
		StringBuilder sb = new StringBuilder();
		while(!(isSpaceChar(b))){ // when nextLine, (isSpaceChar(b) && b != ' ')
			sb.appendCodePoint(b);
			b = readByte();
		}
		return sb.toString();
	}
	
	private char[] ns(int n)
	{
		char[] buf = new char[n];
		int b = skip(), p = 0;
		while(p < n && !(isSpaceChar(b))){
			buf[p++] = (char)b;
			b = readByte();
		}
		return n == p ? buf : Arrays.copyOf(buf, p);
	}
	
	private char[][] nm(int n, int m)
	{
		char[][] map = new char[n][];
		for(int i = 0;i < n;i++)map[i] = ns(m);
		return map;
	}
	
	private int[] na(int n)
	{
		int[] a = new int[n];
		for(int i = 0;i < n;i++)a[i] = ni();
		return a;
	}
	
	private int ni()
	{
		int num = 0, b;
		boolean minus = false;
		while((b = readByte()) != -1 && !((b >= '0' && b <= '9') || b == '-'));
		if(b == '-'){
			minus = true;
			b = readByte();
		}
		
		while(true){
			if(b >= '0' && b <= '9'){
				num = num * 10 + (b - '0');
			}else{
				return minus ? -num : num;
			}
			b = readByte();
		}
	}
	
	private long nl()
	{
		long num = 0;
		int b;
		boolean minus = false;
		while((b = readByte()) != -1 && !((b >= '0' && b <= '9') || b == '-'));
		if(b == '-'){
			minus = true;
			b = readByte();
		}
		
		while(true){
			if(b >= '0' && b <= '9'){
				num = num * 10 + (b - '0');
			}else{
				return minus ? -num : num;
			}
			b = readByte();
		}
	}
	
	private static void tr(Object... o) { System.out.println(Arrays.deepToString(o)); }
}