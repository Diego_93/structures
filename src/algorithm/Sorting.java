package algorithm;

import java.util.Arrays;
import java.util.Random;

import timer.Timer;


public class Sorting {

	/*static int N = 500000;
	static int max = 10000;
	
	
	public static void main(String[] args) {
		Integer[] arr = new Integer[N];
		Random r = new Random();
		int times=0;
		Timer timer = new Timer();
		while(times++ < 20){
			for(int i=0; i<N; i++){
				arr[i]=r.nextInt(max);
			}
			Integer[] copy = arr.clone();
			timer.start();
			quicksort(copy);
			timer.stop();
			System.out.print(timer.getTime()+" ");
			copy = arr.clone();
			timer.start();
			mergesort(copy);
			timer.stop();
			System.out.print(timer.getTime()+" ");
			Integer[] copy2 = arr.clone();
			timer.start();
			Arrays.sort(copy2);
			timer.stop();
			System.out.println(timer.getTime());
			
		}
	}*/
	
	private static <T> String arrayToString(T[] array){
		StringBuilder str = new StringBuilder("[");
		if(array.length>0)
			str.append(array[0]);
		for (int i = 1; i < array.length; i++) {
			str.append(',');
			str.append(' ');
			str.append(array[i].toString());
		}
		str.append(']');
		return str.toString();
	}
	
	public static <T extends Comparable<T>> T getFromQuicksort(T[] array, int index){
		T[] aux = Arrays.copyOf(array, array.length);
		pQuicksort(aux, 0, aux.length-1, index);
		return aux[index];
	}
	
	private static <T extends Comparable<T>> void pQuicksort(T[] array, int left, int right, int index){
		T midValue = array[(right+left)/2];
		int i=left, j=right;
		while(i<=j){
			while(midValue.compareTo(array[i])>0)
				i++;
			while(midValue.compareTo(array[j])<0)
				j--;
			if(i<=j){
				swap(array, i, j);
				i++;
				j--;
			}		
		}
		if(left<right-1){
			if(index<=j) pQuicksort(array, left, j, index);
			if(index>=i) pQuicksort(array, i, right, index);
		}
	}
	
	public static <T extends Comparable<T>> void quicksort(T[] array){
		pQuicksort(array, 0, array.length-1);
	}
	
	private static <T extends Comparable<T>> void pQuicksort(T[] array, int left, int right){
		T midValue = array[(right+left)/2];
		int i=left, j=right;
		while(i<=j){
			while(midValue.compareTo(array[i])>0)
				i++;
			while(midValue.compareTo(array[j])<0)
				j--;
			if(i<=j){
				swap(array, i, j);
				i++;
				j--;
			}		
		}
		if(left<right-1){
			pQuicksort(array, left, j);
			pQuicksort(array, i, right);
		}
	}

	private static <T extends Comparable<T>> void mergesort(T[] array){
		T[] aux = Arrays.copyOf(array, array.length);
		pMergesort(array, aux, 0, array.length-1);
	}
	
	private static <T extends Comparable<T>> void pMergesort(T[] array, T[] aux, int i, int j) {
		if(i<j){
			int mid = (i+j)/2;
			pMergesort(array, aux, i, mid);
			pMergesort(array, aux, mid+1, j);
			int left = i, right = mid+1, k=i;
			while(k <= j){
				if(left>mid){
					array[k++]=aux[right++];
				}else
				if(right>j){
					array[k++]=aux[left++];
				}else
				if(aux[left].compareTo(aux[right])<0){
					array[k++]=aux[left++];
				}else{
					array[k++]=aux[right++];
				}
			}
			for(k=i; k<=j; k++){
				aux[k]=array[k];
			}
		}
		
	}

	private static <T> void swap(T[] array, int i, int j) {
		T aux = array[i];
		array[i]=array[j];
		array[j]=aux;
	}
}

