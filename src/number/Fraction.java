package number;

public class Fraction {

	long numerator, denominator;
	
	public Fraction(long n, long d){
		numerator=n;
		denominator=d;
		refresh();
	}
	
	private void refresh(){
		long mcd=mcd(Math.abs(numerator), Math.abs(denominator));
		if(denominator==0)
			throw new IllegalStateException("div by zero");
		if(denominator<0){
			denominator=-denominator;
			numerator=-numerator;
		}
		numerator/=mcd;
		denominator/=mcd;
	}
	
	private long mcd(long a, long b){
		if(b==0)
			return a;
		return mcd(b, a%b);
	}
	
	static Fraction add(Fraction a, Fraction b){
		return new Fraction(a.numerator*b.denominator+b.numerator*a.denominator, a.denominator*b.denominator);
	}
	
	static Fraction sub(Fraction a, Fraction b){
		return new Fraction(a.numerator*b.denominator-b.numerator*a.denominator, a.denominator*b.denominator);
	}
	
	static Fraction prod(Fraction a, Fraction b){
		return new Fraction(a.numerator*b.numerator, a.denominator*b.denominator);
	}
	
	static Fraction div(Fraction a, Fraction b){
		return new Fraction(a.numerator*b.denominator, a.denominator*b.numerator);
	}
	
	public void inverse(){
		long aux=numerator;
		numerator=denominator;
		denominator=aux;
		refresh();
	}
	
	static Fraction fromContinuedFraction(long[] arr){
		Fraction aux = new Fraction(arr[arr.length-1],1);
		for(int i=arr.length-2; i>=0; i++){
			aux.inverse();
			aux = add(new Fraction(arr[i], 1), aux);
		}
		return aux;
	}
	
	public String toContinuedFractionString(){
		long n=numerator;
		long d=denominator;
		String ans="";
		if(n<0){
			ans+="-";
			n=-n;
		}
		long div = n/d;
		ans+=div;
		n%=d;
		while(n>0){
			long aux=n;
			n=d;
			d=aux;
			div = n/d;
			ans+=" "+div;
			n%=d;
		}
		return ans;
	}
}
