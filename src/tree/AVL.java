package tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AVL<T> {

	private Node<T> root;
	private int size = 0;
	private Comparator<? super T> cmp;
	private int cantOperations;

	private static class Node<T> {
		T value;
		Node<T> left, right;
		int height;
		int size;

		public Node() {
			this(null, null, null);
		}

		public Node(T value) {
			this(value, null, null);
		}

		public Node(T value, Node<T> left, Node<T> right) {
			this.value = value;
			this.left = left;
			this.right = right;
			calculateValues(this);
		}
	}

	public AVL(Comparator<T> cmp) {
		this();
		this.cmp = cmp;
	}

	private AVL(Node<T> root) {
		this.root = root;
	}

	private AVL() {
		this.root = null;
	}

	private AVL(Node<T> root, Comparator<T> cmp) {
		this(root);
		this.cmp = cmp;
	}

	private AVL(Node<T> root, Comparator<T> cmp, int size) {
		this(root);
		this.cmp = cmp;
		this.size = size;
	}

	private static <T> Node<T> balanceTree(Node<T> root) {
		if (height(root.left) - height(root.right) > 1) {
			if (height(root.left.left) - height(root.left.right) >= 0) {
				root = rotateRight(root);
			} else {
				root.left = rotateLeft(root.left);
				root = rotateRight(root);
			}
		} else if (height(root.left) - height(root.right) < -1) {
			if (height(root.right.left) - height(root.right.right) <= 0) {
				root = rotateLeft(root);
			} else {
				root.right = rotateRight(root.right);
				root = rotateLeft(root);
			}
		}
		calculateValues(root);
		return root;
	}

	private static <T> Node<T> rotateRight(Node<T> root) {
		Node<T> aux = root.left;
		root.left = root.left.right;
		aux.right = root;
		calculateValues(aux);
		calculateValues(aux.right);
		return aux;
	}

	private static <T> Node<T> rotateLeft(Node<T> root) {
		Node<T> aux = root.right;
		root.right = root.right.left;
		aux.left = root;
		calculateValues(aux);
		calculateValues(aux.left);
		return aux;
	}

	private static <T> void calculateValues(Node<T> root) {
		calculateHeight(root);
		calculateSize(root);
	}

	private static <T> void calculateHeight(Node<T> root) {
		root.height = Math.max(height(root.left), height(root.right)) + 1;
	}

	private static <T> int height(Node<T> root) {
		if (root == null) {
			return -1;
		}
		return root.height;
	}

	private static <T> void calculateSize(Node<T> root) {
		root.size = size(root.left) + size(root.right) + 1;
	}

	private static <T> int size(Node<T> root) {
		if (root == null) {
			return 0;
		}
		return root.size;
	}

	/**
	 * O(lg n*)
	 */
	public boolean add(T key) {
		int auxSize = size;
		root = add(root, key);
		return auxSize!=size;
	}

	private Node<T> add(Node<T> root, T key) {
		if (root == null) {
			size++;
			cantOperations++;
			return new Node<T>(key);
		}
		if (cmp.compare(root.value, key) > 0) {
			root.left = add(root.left, key);
		} else if (cmp.compare(root.value, key) < 0) {
			root.right = add(root.right, key);
		}
		return balanceTree(root);
	}

	/**
	 * O(lg n*)
	 */
	public boolean remove(T key) {
		int auxSize = size;
		root = remove(root, key);
		return auxSize!=size;
	}

	private Node<T> remove(Node<T> root, T key) {
		if (root == null) {
			return null;
		}
		if (root.value.equals(key)) {
			size--;
			cantOperations++;
			if (root.left == null) {
				return root.right;
			}
			root.left = cutAndPasteMax(root.left, root);
		}
		if (cmp.compare(root.value, key) > 0) {
			root.left = remove(root.left, key);
		} else if (cmp.compare(root.value, key) < 0) {
			root.right = remove(root.right, key);
		}
		return balanceTree(root);
	}

	private Node<T> cutAndPasteMax(Node<T> root, Node<T> to) {
		if (root.right == null) {
			to.value = root.value;
			return root.left;
		}
		root.right = cutAndPasteMax(root.right, to);
		return balanceTree(root);
	}

	/**
	 * O(lg n)
	 */
	public boolean contains(T key) {
		return contains(root, key);
	}

	private boolean contains(Node<T> root, T key) {
		if (root == null) {
			return false;
		}
		if (cmp.compare(root.value, key) > 0) {
			return contains(root.left, key);
		} else if (cmp.compare(root.value, key) < 0) {
			return contains(root.right, key);
		}
		return true;
	}

	/**
	 * O(1)
	 */
	public int size() {
		return size;
	}

	/**
	 * O(n)
	 */
	public static AVL<Long> toAVL(long[] arr) {
		return new AVL<Long>(toAVL(arr, 0, arr.length), (a, b) -> Long.compare(
				a, b), arr.length);
	}

	private static Node<Long> toAVL(long[] arr, int left, int right) {
		if (left == right)
			return null;
		int mid = (left + right) / 2;
		return new Node<Long>(arr[mid], toAVL(arr, left, mid), toAVL(arr,
				mid + 1, right));
	}

	/**
	 * O(n)
	 */
	public int leaves() {
		return leaves(root);
	}

	private int leaves(Node<T> root) {
		if (root == null)
			return 0;
		if (root.left == null && root.right == null)
			return 1;
		return leaves(root.left) + leaves(root.right);
	}

	/**
	 * O(log n)
	 */
	public List<T> ancestorsOf(T key) {
		List<T> list = new LinkedList<T>();
		ancestorsOf(root, key, list);
		return list;
	}

	private boolean ancestorsOf(Node<T> root, T key, List<T> list) {
		if (root == null)
			return false;
		if (root.value.equals(key)) {
			return true;
		}
		boolean aux = false;
		if (cmp.compare(root.value, key) > 0) {
			aux = ancestorsOf(root.left, key, list);
		} else if (cmp.compare(root.value, key) < 0) {
			aux = ancestorsOf(root.right, key, list);
		}
		if (aux) {
			list.add(root.value);
		}
		return aux;
	}

	/**
	 * O(n)
	 */
	public List<T> predecessorsOf(T key) {
		List<T> list = new LinkedList<T>();
		predecessorsOf(root, key, list);
		return list;
	}

	private void predecessorsOf(Node<T> root, T key, List<T> list) {
		if (root == null)
			return;
		if (root.value.equals(key)) {
			print(root.left, list);
			print(root.right, list);
			return;
		}
		if (cmp.compare(root.value, key) > 0) {
			predecessorsOf(root.left, key, list);
		} else if (cmp.compare(root.value, key) < 0) {
			predecessorsOf(root.right, key, list);
		}
		return;
	}

	private void print(Node<T> root, List<T> list) {
		if (root == null)
			return;
		list.add(root.value);
		print(root.left, list);
		print(root.right, list);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (o instanceof AVL) {
			return equals((AVL<?>) o);
		}
		return false;
	}

	/**
	 * O(n)
	 */
	public boolean equals(AVL<T> tree) {
		return equals(root, tree.root);
	}

	private boolean equals(Node<T> r1, Node<T> r2) {
		if (r1 == null && r2 == null)
			return true;
		if (r1 == null || r2 == null || !r1.value.equals(r2.value))
			return false;
		return equals(r1.left, r2.left) && equals(r1.right, r2.right);
	}

	/**
	 * O(1) - Just check the first 3 elements and the size.
	 */
	public int hashCode() {
		if (root == null)
			return 1;
		return (7 * size + 11 * root.value.hashCode() + 17
				* (root.left == null ? 0 : root.left.hashCode()) + 19 * 17 * (root.right == null ? 0
				: root.right.hashCode()));
	}

	private class myIterator<S> implements Iterator<S> {

		int cantOp = cantOperations;
		Iterator<S> itt;

		public void selectOrder(List<S> arr) {
			itt = arr.iterator();
		}

		@Override
		public boolean hasNext() {
			if (cantOp != cantOperations)
				throw new IllegalStateException();
			return itt.hasNext();
		}

		@Override
		public S next() {
			if (cantOp != cantOperations)
				throw new IllegalStateException();
			return itt.next();
		}

		@Override
		public void remove() {
			if (cantOp != cantOperations)
				throw new IllegalStateException();
			itt.remove();
		}
	}
	
	/**
	 * O(n)
	 */
	public List<T> preOrderList(){
		List<T> arr = new LinkedList<T>();
		toPreOrder(root, arr);
		return arr;
	}
	
	/**
	 * O(n*)
	 */
	public ArrayList<T> preOrderArrayList(){
		ArrayList<T> arr = new ArrayList<T>();
		toPreOrder(root, arr);
		return arr;
	}
	
	/**
	 * O(n)
	 */
	public List<T> postOrderList(){
		List<T> arr = new LinkedList<T>();
		toPostOrder(root, arr);
		return arr;
	}
	
	/**
	 * O(n*)
	 */
	public ArrayList<T> postOrderArrayList(){
		ArrayList<T> arr = new ArrayList<T>();
		toPostOrder(root, arr);
		return arr;
	}
	
	/**
	 * O(n)
	 */
	public List<T> inOrderList(){
		List<T> arr = new LinkedList<T>();
		toInOrder(root, arr);
		return arr;
	}
	
	/**
	 * O(n*)
	 */
	public ArrayList<T> inOrderArrayList(){
		ArrayList<T> arr = new ArrayList<T>();
		toInOrder(root, arr);
		return arr;
	}

	/**
	 * O(n)
	 */
	public Iterator<T> preOrder() {
		myIterator<T> it = new myIterator<T>();
		List<T> arr = new LinkedList<T>();
		toPreOrder(root, arr);
		it.selectOrder(arr);
		return it;
	}

	/**
	 * O(n)
	 */
	public Iterator<T> inOrder() {
		myIterator<T> it = new myIterator<T>();
		List<T> arr = new LinkedList<T>();
		toInOrder(root, arr);
		it.selectOrder(arr);
		return it;
	}

	/**
	 * O(n)
	 */
	public Iterator<T> postOrder() {
		myIterator<T> it = new myIterator<T>();
		List<T> arr = new LinkedList<T>();
		toPostOrder(root, arr);
		it.selectOrder(arr);
		return it;
	}

	private void toPreOrder(Node<T> root, List<T> list) {
		if (root == null)
			return;
		list.add(root.value);
		toPreOrder(root.left, list);
		toPreOrder(root.right, list);
	}

	private void toInOrder(Node<T> root, List<T> list) {
		if (root == null)
			return;
		toInOrder(root.left, list);
		list.add(root.value);
		toInOrder(root.right, list);
	}

	private void toPostOrder(Node<T> root, List<T> list) {
		if (root == null)
			return;

		toPostOrder(root.left, list);
		toPostOrder(root.right, list);
		list.add(root.value);
	}
	
	/**
	 * O(n)
	 */
	public List<T> elementsInRange(T min, T max){
		List<T> ans= new LinkedList<T>();
		if (min==null || max==null || (cmp.compare(max, min)<0)){
			throw new IllegalArgumentException();
		}
		elementsInRange(root,min,max,ans);
		return ans;
	}
	
	/**
	 * O(n*)
	 */
	public ArrayList<T> elementsInRangeArray(T min, T max){
		ArrayList<T> ans= new ArrayList<T>();
		if (min==null || max==null || (cmp.compare(max, min)<0)){
			throw new IllegalArgumentException();
		}
		elementsInRange(root,min,max,ans);
		return ans;
	}
	
	private void elementsInRange(Node<T> root,T min, T max, List<T> list){
		if(root==null)
			return;
		if (cmp.compare(min, root.value)<=0 && cmp.compare(root.value, max)<=0){
			list.add(root.value);
		}
		if (cmp.compare(min, root.value)<0){
			elementsInRange(root.left, min, max, list);
		}
		if (cmp.compare(max, root.value)>0){
			elementsInRange(root.right, min, max, list);
		}
	}
	
	/**
	 * O(lg n)
	 */
	public T getKElement(int k){
		if(k<0 || k>=size)
			throw new ArrayIndexOutOfBoundsException();
		return getKElement(root, k);
	}
	
	private T getKElement(Node<T> root, int k) {
		if(root==null)
			return null;
		int leftElements = size(root.left);
		if(leftElements>k) return getKElement(root.left, k);
		k-=leftElements;
		if(k==0)
			return root.value;
		return getKElement(root.right, k-1);
	}

	/**
	 * O(lg n)
	 */
	public int position(T key){
		return position(root, 0, key);
	}
	
	private int position(Node<T> root, int offset, T key){
		if(root==null)
			return -1;
		if(root.value.equals(key)){
			return offset+size(root.left);
		}
		if(cmp.compare(key,root.value)<0){
			return position(root.left, offset, key);
		}else{
			return position(root.right, offset+size(root.left)+1, key);
		}
	}
	
	/**
	 * O(lg n)
	 */
	public int lowerElements(T key){
		return lowerElements(root, key);
	}
	
	private int lowerElements(Node<T> root, T key){
		if(root==null)
			return 0;
		if(root.value.equals(key)){
			return size(root.left);
		}
		if(cmp.compare(key,root.value)<0){
			return lowerElements(root.left, key);
		}else{
			return size(root.left)+1+lowerElements(root.right, key);
		}
	}

	/*--------------------------------------------------------------------------------------*/

	/*
	 * public boolean isBST(Comparator<T> cmp){ return
	 * isBST(root,cmp,null,null); };
	 * 
	 * protected boolean isBST(Node<T> root,Comparator<T> cmp,T min,T max){
	 * if(root==null){ return true; } if (min!=null && cmp.compare(min,
	 * root.value)>0){ return false; } if (max!=null && cmp.compare(max,
	 * root.value)<0){ return false; } return
	 * isBST(root.left,cmp,min,root.value)
	 * &&isBST(root.right,cmp,root.value,max); }
	 */

	/*
	 * public boolean isAVL(){ return (isAVL(root)!=-2); };
	 * 
	 * private int isAVL(Node<T> root){ if(root==null){ return -1; } int
	 * left,right; if ((left=isAVL(root.left))==-2 ||
	 * (right=isAVL(root.right))==-2 || Math.abs(right-left)>1){ return -2; }
	 * return Math.max(left, right)+1; }
	 */
	
	public void printByLevel() {

		Queue<Node<T>> cola = new LinkedList<Node<T>>();
		cola.add(root);
		while (!cola.isEmpty()) {
			Node<T> aux = cola.remove();
			System.out.println(aux.value);
			if (aux.left != null)
				cola.add(aux.left);
			if (aux.right != null)
				cola.add(aux.right);
		}
	}

	protected void printByLevel(Node<T> root) {
		Queue<Node<T>> cola = new LinkedList<Node<T>>();
		cola.add(root);
		while (!cola.isEmpty()) {
			Node<T> aux = cola.remove();
			System.out.println(aux.value);
			if (aux.left != null)
				cola.add(aux.left);
			if (aux.right != null)
				cola.add(aux.right);
		}
	}

	protected static <T> void printNode(Node<T> root) {
		int maxLevel = maxLevel(root);
		printNodeInternal(Collections.singletonList(root), 1, maxLevel);
	}

	private static <T> void printNodeInternal(List<Node<T>> nodes, int level,
			int maxLevel) {
		if (nodes.isEmpty() || isAllElementsNull(nodes))
			return;
		int floor = maxLevel - level;
		int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
		int firstSpaces = (int) Math.pow(2, (floor)) - 1;
		int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;
		printWhitespaces(firstSpaces);
		List<Node<T>> newNodes = new ArrayList<Node<T>>();
		for (Node<T> node : nodes) {
			if (node != null) {
				System.out.print(node.value);
				newNodes.add(node.left);
				newNodes.add(node.right);
			} else {
				newNodes.add(null);
				newNodes.add(null);
				System.out.print(" ");
			}
			printWhitespaces(betweenSpaces);
		}
		System.out.println("");
		for (int i = 1; i <= endgeLines; i++) {
			for (int j = 0; j < nodes.size(); j++) {
				printWhitespaces(firstSpaces - i);
				if (nodes.get(j) == null) {
					printWhitespaces(endgeLines + endgeLines + i + 1);
					continue;
				}
				if (nodes.get(j).left != null)
					System.out.print("/");
				else
					printWhitespaces(1);
				printWhitespaces(i + i - 1);
				if (nodes.get(j).right != null)
					System.out.print("\\");
				else
					printWhitespaces(1);
				printWhitespaces(endgeLines + endgeLines - i);
			}
			System.out.println("");
		}
		printNodeInternal(newNodes, level + 1, maxLevel);
	}

	private static void printWhitespaces(int count) {
		for (int i = 0; i < count; i++)
			System.out.print(" ");
	}

	private static <T> int maxLevel(Node<T> node) {
		if (node == null)
			return 0;
		return Math.max(maxLevel(node.left), maxLevel(node.right)) + 1;
	}

	private static <T> boolean isAllElementsNull(List<T> list) {
		for (Object object : list) {
			if (object != null)
				return false;
		}
		return true;
	}

	public void graph() {
		printNode(root);
	}
}
