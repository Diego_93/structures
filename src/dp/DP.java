package dp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;

public class DP<K, D> {

	private D[] mem1;
	private DP1Interface<D> dp1Interface;
	private D[][] mem2;
	private DP2Interface<D> dp2Interface;
	private D[][][] mem3;
	private DP3Interface<D> dp3Interface;
	private Map<K, D> map;
	private DPMapInterface<K, D> dpMapInterface;
	private boolean filling = false;

	public DP(DPMapInterface<K, D> dpint) {
		map = new HashMap<K, D>();
		dpMapInterface = dpint;
	}

	public DP(int D1, DP1Interface<D> dpint) {
		mem1 = (D[]) new Object[D1];
		dp1Interface = dpint;
	}

	public DP(int D1, int D2, DP2Interface<D> dpint) {
		mem2 = (D[][]) new Object[D1][D2];
		dp2Interface = dpint;
	}

	public DP(int D1, int D2, int D3, DP3Interface<D> dpint) {
		mem3 = (D[][][]) new Object[D1][D2][D3];
		dp3Interface = dpint;
	}

	public D get(K data) {
		if (filling) {
			D caseBase = dpMapInterface.caseBase(data);
			if (caseBase != null)
				return caseBase;
			D aux = map.get(data);
			if (aux == null)
				return dpMapInterface.neutralElement();
			return aux;
		} else {
			D caseBase = dpMapInterface.caseBase(data);
			if (caseBase != null)
				return caseBase;
			D aux = map.get(data);
			if (aux == null) {
				aux = dpMapInterface.get(data);
				map.put(data, aux);
			}
			return aux;
		}
	}

	public D get(int i) {
		if (filling) {
			D caseBase = dp1Interface.caseBase(i);
			if (caseBase != null)
				return caseBase;
			if (mem1[i] == null)
				return dp1Interface.neutralElement();
			return mem1[i];
		} else {
			D caseBase = dp1Interface.caseBase(i);
			if (caseBase != null)
				return caseBase;
			if (mem1[i] == null)
				mem1[i] = dp1Interface.get(i);
			return mem1[i];
		}
	}

	public D get(int i, int j) {
		if (filling) {
			D caseBase = dp2Interface.caseBase(i, j);
			if (caseBase != null)
				return caseBase;
			if (mem2[i][j] == null)
				return dp2Interface.neutralElement();
			return mem2[i][j];
		} else {
			D caseBase = dp2Interface.caseBase(i, j);
			if (caseBase != null)
				return caseBase;
			if (mem2[i][j] == null)
				mem2[i][j] = dp2Interface.get(i, j);
			return mem2[i][j];
		}
	}

	public D get(int i, int j, int k) {
		if (filling) {
			D caseBase = dp3Interface.caseBase(i, j, k);
			if (caseBase != null)
				return caseBase;
			if (mem3[i][j][k] == null)
				return dp3Interface.neutralElement();
			return mem3[i][j][k];
		} else {
			D caseBase = dp3Interface.caseBase(i, j, k);
			if (caseBase != null)
				return caseBase;
			if (mem3[i][j][k] == null)
				mem3[i][j][k] = dp3Interface.get(i, j, k);
			return mem3[i][j][k];
		}
	}

	public void fillMapByOrder(Iterator<K> q) {
		filling = true;
		while (!q.hasNext()) {
			K data = q.next();
			map.put(data, dpMapInterface.get(data));
		}
		filling = false;
	}

	public void fillDP1ByOrder(Iterator<Integer> q) {
		filling = true;
		while (!q.hasNext()) {
			int i = q.next();
			mem1[i] = dp1Interface.get(i);
		}
		filling = false;
	}

	public void fillDP2ByOrder(Iterator<Pair<Integer, Integer>> q) {
		filling = true;
		while (!q.hasNext()) {
			Pair<Integer, Integer> p = q.next();
			int i=p.first;
			int j=p.second;
			mem2[i][j] = dp2Interface.get(i, j);
		}
		filling = false;
	}
	
	public void fillDP3ByOrder(Iterator<Triple<Integer, Integer, Integer>> q) {
		filling = true;
		while (!q.hasNext()) {
			Triple<Integer, Integer, Integer> p = q.next();
			int i=p.first;
			int j=p.second;
			int k=p.third;
			mem3[i][j][k] = dp3Interface.get(i, j, k);
		}
		filling = false;
	}

	static public interface DP1Interface<T> {
		public T get(int i);

		public T caseBase(int i);

		public T neutralElement();
	}

	static public interface DP2Interface<T> {
		public T get(int i, int j);

		public T caseBase(int i, int j);

		public T neutralElement();
	}

	static public interface DP3Interface<T> {
		public T get(int i, int j, int k);

		public T caseBase(int i, int j, int k);

		public T neutralElement();
	}

	static public interface DPMapInterface<K, T> {
		public T get(K data);

		public T caseBase(K data);

		public T neutralElement();
	}

	static public class Pair<A, B> {

		public A first;
		public B second;

		public Pair() {
			first = null;
			second = null;
		}

		public Pair(A first, B second) {
			super();
			this.first = first;
			this.second = second;
		}
	}

	static public class Triple<A, B, C> {

		public A first;
		public B second;
		public C third;

		public Triple() {
			first = null;
			second = null;
			third = null;
		}

		public Triple(A first, B second, C third) {
			super();
			this.first = first;
			this.second = second;
			this.third = third;
		}
	}
}
