package list;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SortedList<T> implements List<T>{

	private Node first;
	private int size;
	private Comparator<T> cmp;
	
	public SortedList(Comparator<T> cmp) {
		this.cmp=cmp;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			Node node = first;
			@Override
			public boolean hasNext() {
				return node != null;
			}
			@Override
			public T next() {
				T elem = node.elem;
				node = node.next;
				return elem;
			}
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public T get(int index) {
		if(index >= size || index<0) throw new IndexOutOfBoundsException();
		return get(first, index);
	}
	
	private T get(Node node, int index){
		if(index == 0) return node.elem;
		return get(node.next, index-1);
	}
	
	public boolean remove(Object elem){
		int ssize=size;
		first = remove(first, elem);
		return ssize!=size; 
	}
	
	private Node remove(Node node, Object elem){
		if(node == null) return null;
		if(node.elem.equals(elem)){
			size--;
			return node.next;
		}
		node.next = remove(node.next, elem);
		return node;
	}
	
	@Override
	public T remove(int index) {
		if(index >= size || index<0) throw new IndexOutOfBoundsException();
		T[] ans=(T[])new Object[1];
		first=remove(first, index, ans);
		return ans[0];
	}
	
	private Node remove(Node node, int index, T[] ans){
		if(index==0){
			ans[0]=node.elem;
			size--;
			return node.next;
		}
		node.next=remove(node.next,index-1,ans);
		return node;
	}
	
	public int size() {
		return size;
	}

	/**
	 * 
	 * Insert the specified element such at a position that the list remain orderly.
	 * 
	 * @return false if the element to be insert is null
	 * 
	 * @param elem - element to be inserted.
	 */
	public boolean add(T elem) {
		if(elem==null)
			return false;
		first = add(elem, first);
		size++;
		return true;
	}
	
	private Node add(T elem, Node node){
		if(node == null) return new Node(elem, null);
		
		if(cmp.compare(elem,node.elem) < 0)
			return new Node(elem, node);
		else
			node.next = add(elem, node.next);
		
		return node;
	}
		
	/**
	 * 	Unsupported Operation in SortedList
	 * 	@throws UnsupportedOperationException
	 */
	public void add(int index, T element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		int ssize= size;
		for (T t : c) {
			add(t);
		}
		return ssize!=size;
	}

	/**
	 *  Unsupported Operation in SortedList
	 *  @throws UnsupportedOperationException
	 */
	public boolean addAll(int index, Collection<? extends T> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		first=null;
		size=0;
	}

	public boolean contains(Object o) {
		return contains(first, o);
	}

	private boolean contains(Node node, Object o) {
		if(node==null)
			return false;
		if(node.elem.equals(o))
			return true;
		return contains(node.next, o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for(Object o : c)
			if(!contains(o))
				return false;
		return true;
	}

	@Override
	public int indexOf(Object o) {
		int index=0;
		for(T t : this){
			if(t.equals(o))
				return index;
			index++;
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public int lastIndexOf(Object o) {
		int index=0;
		int answer=-1;
		for(T t : this){
			if(t.equals(o))
				answer=index;
			index++;
		}
		return answer;
	}

	/**
	 *  Unsupported Operation in SortedList
	 *  @throws UnsupportedOperationException
	 */
	public ListIterator<T> listIterator() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 *  Unsupported Operation in SortedList
	 *  @throws UnsupportedOperationException
	 */
	public ListIterator<T> listIterator(int index) {
		throw new UnsupportedOperationException();
	}


	@Override
	public boolean removeAll(Collection<?> c) {
		int ssize=size;
		for (Object o : c) {
			remove(o);
		}
		return ssize!=size;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		int ssize=size;
		first=retainAll(first,c);
		return ssize!=size;
	}

	private Node retainAll(Node node, Collection<?> c) {
		if(node==null)
			return null;
		node=retainAll(node.next, c);
		if(c.contains(node.elem)){
			size--;
			return node.next;
		}
		return node;		
	}

	
	
	
	@Override
	public int hashCode() {
		return size*31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof List))
			return false;
		List<T> other = (List<T>) obj;
		Iterator<T> it1 = this.iterator(), it2=other.iterator();
		while(it1.hasNext() && it2.hasNext()){
			if(!it1.next().equals(it2.next()))
				return false;
		}
		if(it1.hasNext() || it2.hasNext())
			return false;
		return true;
	}

	@Override
	public String toString() {
		if(size==0)
			return "[]";
		StringBuffer aux= new StringBuffer();
		aux.append('[');
		for (T t : this) {
			aux.append(t.toString());
			aux.append(',');
		}
		aux.replace(aux.length()-1, aux.length(), "]");
		return aux.toString();
	}
	/**
	 *  Unsupported Operation in SortedList
	 *  @throws UnsupportedOperationException
	 */
	public T set(int index, T element) {
		throw new UnsupportedOperationException();
	}

	/**
	 *  Unsupported Operation in SortedList
	 *  @throws UnsupportedOperationException
	 */
	public List<T> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray() {
		Object[] array= new Object[size];
		int i=0;
		for (T t : this) {
			array[i++]=t;
		}
		return array;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public <E> E[] toArray(E[] a) {
		if(a.length>=size){
			int i=0;
			for (T t : this) {
				a[i++]=(E)t;
			}
			while(i<a.length)
				a[i++]=null;
			return a;
		}else{
			E[] ans=Arrays.copyOf(a, size);
			int i=0;
			for(T t: this){
				ans[i++]=(E)t;
			}
			return ans;
		}

	}
	
	
	private class Node {
		T elem;
		Node next;
		
		public Node(T elem, Node next){
			this.elem = elem;
			this.next = next;
		}
	}


}
