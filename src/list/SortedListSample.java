package list;

import java.util.Comparator;
import java.util.List;

public class SortedListSample {
	
	public static void main(String[] args) {
		List<Integer> list= new SortedList<Integer>(new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
		System.out.println("add 3: "+list.add(3));
		printList(list);
		System.out.println("add 4: "+list.add(4));
		printList(list);
		System.out.println("add 8: "+list.add(8));
		printList(list);
		System.out.println("add 1: "+list.add(1));
		printList(list);
		System.out.println("add 0: "+list.add(0));
		printList(list);
		System.out.println("add null: "+list.add(null));
		printList(list);
		System.out.println("add -2: "+list.add(-2));
		printList(list);
		System.out.println("remove 2: "+list.remove(2));
		printList(list);
		System.out.println("add 0: "+list.add(0));
		printList(list);
		System.out.println("get 2: "+list.get(2));
		System.out.println("is empty: "+list.isEmpty());
		System.out.println("contains 4: "+list.contains(4));
		System.out.println("size: "+list.size());
		System.out.println("index of 0: "+list.indexOf(0));
		System.out.println("last index of 0: "+list.lastIndexOf(0));
		System.out.println("to array:");
		printArray(list.toArray(new Integer[0]));
		System.out.println(list);
		List<Integer> list2 = new SortedList<Integer>(new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
		list2.add(8);
		list2.add(4);
		list2.add(3);
		list2.add(0);
		list2.add(-2);
		list2.add(0);
		System.out.println("equals: "+list.equals(list2));
		System.out.println("remove object 0: "+list2.remove(new Integer(0)));
		System.out.println("list2: "+list2);
		System.out.println("equals: "+list.equals(list2));
		list2.clear();
		System.out.println("clear list2: "+list2);
	}
	
	private static<T> void printArray(T[] array){
		for (T t : array) {
			System.out.print(t+" ");
		}
		System.out.println();
	}
	
	private static<T> void printList(List<T> list){
		for (T t : list) {
			System.out.print(t+" ");
		}
		System.out.println();
	}

}
