package auxiliar;

public class BitOperations {

	public static int onB(int value, int index){
    	return value|(1<<index);
    }
    
    public static int offB(int value, int index){
    	int aux = (1<<index);
    	return value & (~aux);
    }
    
    public static boolean getB(int value, int index){
    	return (value>>index)%2==1;
    }
}
