package auxiliar;


public class Searching {
	
	public static void main(String[] args) {
		System.out.println(ternary(0, 200));
	}
	
	static double EPS = 1e-9;
	
	public static int binary(int left, int right){
    	if(left+1>=right)
    		return left;
    	int m = (left+right)/2;
    	if(condition(m)){
    		return binary(m, right);
    	}else{
    		return binary(left, m);
    	}
    }
    
    public static boolean condition(int m){
    	return true;
    }
    
    public static double value(double m){
    	return 10*m+4*m*m-m*m*m;
    }
    
    public static double ternary(double left, double right){
    	System.out.println(left+" "+right);
    	if(right-left<=EPS)
    		return left;
    	double m1 = (left*2+right)/3;
    	double m2 = (left+right*2)/3;
    	if(value(m1)>=value(m2)){
    		return ternary(left, m2);
    	}else{
    		return ternary(m1, right);
    	}
    }
}
