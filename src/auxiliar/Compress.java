package auxiliar;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Compress {
	
	Map<Long, Integer> map;
	Map<Integer, Long> umap;
	
	public Compress(long[] arr1){
		long[] arr = Arrays.copyOf(arr1, arr1.length);
		Arrays.sort(arr);
		int a=0;
		long last=arr[0]-1;
		map = new HashMap<Long, Integer>();
		umap = new HashMap<Integer, Long>();
		for(int i=0; i<arr.length; i++){
			if(last==arr[i]){
				continue;
			}
			map.put(arr[i], a);
			umap.put(a, arr[i]);
			a++;
			last=arr[i];
		}
	}
	
	public Integer c(long a){
		return map.get(a);
	}
	
	public Long u(int a){
		return umap.get(a);
	}
	
	public Integer compress(long a){
		return map.get(a);
	}
	
	public Long uncompress(int a){
		return umap.get(a);
	}

}
