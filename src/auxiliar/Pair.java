package auxiliar;

public class Pair<T extends Comparable<T>, S extends Comparable<S>> implements Comparable<Pair<T, S>>{
	
	public T first;
	public S second;
	
	public Pair(T first, S second){
		this.first = first;
		this.second = second;
	}
	
	@Override
	public int compareTo(Pair<T, S> other) {
		int cmp = first.compareTo(other.first);
		if(cmp==0)
			return second.compareTo(other.second);
		return cmp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
}
