package auxiliar;

public class ChangeBases {

	
	static long toDecimal(String str, int base){
		long ans=0;
		for(int i=0; i<str.length(); i++){
			char ch=str.charAt(i);
			long num;
			if(ch<='9' && ch>='0'){
				num=ch-'0';
			}else{
				num=ch-'A';
			}
			ans=(ans*base)+num;
		}
		return ans;
	}
	
	static String fromDecimal(long decimal, int base){
		String ans="";
		if(decimal==0)
			return ans;
		int num = (int)decimal%base;
		char ch;
		if(num<10){
			ch=(char)(num+'0');
		}else{
			if(num<36){
				ch=(char)(num-10+'a');
			}else{
				ch=(char)(num-10+'A');
			}
		}
		return fromDecimal(decimal/base, base)+ch;
	}
}
